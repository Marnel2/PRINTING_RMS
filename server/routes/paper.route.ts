import express from "express";
import { isAuthenticated } from "../middlewares/auth";
import {
	addNewPaper,
	addPaperBrand,
	addPaperCategory,
	createPaperConsumptionReport,
	getCategories,
	getPaperBrands,
	getPaperReport,
	getPapers,
} from "../controllers/paper.controller";

const paperRouter = express.Router();

paperRouter.post("/add-category", isAuthenticated, addPaperCategory);
paperRouter.post("/add-brand", isAuthenticated, addPaperBrand);

paperRouter.post("/add-paper", isAuthenticated, addNewPaper);

paperRouter.get("/categories", isAuthenticated, getCategories);

paperRouter.get("/get-brand/:categoryId", isAuthenticated, getPaperBrands);

paperRouter.post("/get-papers", isAuthenticated, getPapers);

paperRouter.post(
	"/paper-report",
	isAuthenticated,
	createPaperConsumptionReport
);

paperRouter.get("/paper-report", isAuthenticated, getPaperReport);

export default paperRouter;
