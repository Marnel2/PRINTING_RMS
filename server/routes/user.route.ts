import express from "express";
import {
	createUser,
	fetchUser,
	loginUser,
	logout,
	refreshToken,
} from "../controllers/user.controller";
import { isAuthenticated } from "../middlewares/auth";

const userRouter = express.Router();

userRouter.post("/create-user", createUser);
userRouter.post("/login", loginUser);

userRouter.get("/me", isAuthenticated, fetchUser);

userRouter.post("/logout", logout);
userRouter.post("/refresh", refreshToken);

export default userRouter;
