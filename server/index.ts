import express from "express";
import prisma from "./prisma/prisma.service";
import cors from "cors";
import cookieParser from "cookie-parser";
import colors from "colors";

import { corsOptions } from "./config/cors";
import { ErrorMiddleware } from "./middlewares/error";
import userRouter from "./routes/user.route";
import paperRouter from "./routes/paper.route";

const app = express();

const PORT = process.env.PORT || 5001;

app.use(cors(corsOptions));

// body parser
app.use(express.urlencoded({ extended: true }));
app.use(express.json({ limit: "50mb" }));

// cookie parser
app.use(cookieParser());

app.use("/api/v1", userRouter, paperRouter);

async function main() {
	try {
		await prisma.$connect();
		console.log(colors.green("Connected to Database!"));

		app.listen(PORT, () => {
			console.log(colors.cyan.underline(`Server is running on port ${PORT}`));
		});

		await new Promise((resolve) => setTimeout(resolve, 100));
	} catch (error) {
		console.error("Failed to start the server:", error);
		process.exit(1);
	} finally {
		await prisma.$disconnect();
	}
}

main()
	.then(async () => {
		await prisma.$disconnect();
	})
	.catch(async (e) => {
		console.error(e);
		await prisma.$disconnect();
		process.exit(1);
	});

app.use(ErrorMiddleware);
