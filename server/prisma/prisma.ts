import { PrismaClient } from "@prisma/client";
import hashPassword from "../middlewares/hashPassword";

const prisma = new PrismaClient();

export default prisma;
