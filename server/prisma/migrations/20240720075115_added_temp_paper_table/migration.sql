-- CreateEnum
CREATE TYPE "PaperServiceType" AS ENUM ('Print', 'Copy', 'Consumed', 'Damaged');

-- CreateTable
CREATE TABLE "PaperConsumption" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "letterPaperCount" INTEGER NOT NULL,
    "a4PaperCount" INTEGER NOT NULL,
    "folioPaperCount" INTEGER NOT NULL,
    "paperServiceType" "PaperServiceType" NOT NULL,
    "remarks" TEXT NOT NULL,

    CONSTRAINT "PaperConsumption_pkey" PRIMARY KEY ("id")
);
