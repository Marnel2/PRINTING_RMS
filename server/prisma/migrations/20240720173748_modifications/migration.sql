-- AlterTable
ALTER TABLE "PaperConsumption" ALTER COLUMN "letterPaperCount" SET DEFAULT 0,
ALTER COLUMN "a4PaperCount" SET DEFAULT 0,
ALTER COLUMN "folioPaperCount" SET DEFAULT 0;
