/*
  Warnings:

  - You are about to drop the column `paperServiceType` on the `PaperConsumption` table. All the data in the column will be lost.
  - You are about to drop the column `quanity` on the `PaperConsumption` table. All the data in the column will be lost.
  - Added the required column `quantity` to the `PaperConsumption` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "PaperConsumption" DROP COLUMN "paperServiceType",
DROP COLUMN "quanity",
ADD COLUMN     "quantity" INTEGER NOT NULL;
