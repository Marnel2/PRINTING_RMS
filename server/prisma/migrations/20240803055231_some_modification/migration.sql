/*
  Warnings:

  - Added the required column `categoryId` to the `PaperBrands` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "PaperBrands" ADD COLUMN     "categoryId" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "PaperCategory" ALTER COLUMN "categoryName" SET DEFAULT 'BondPaper';

-- AddForeignKey
ALTER TABLE "PaperBrands" ADD CONSTRAINT "PaperBrands_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "PaperCategory"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
