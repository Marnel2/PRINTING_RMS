/*
  Warnings:

  - You are about to drop the column `a4PaperCount` on the `PaperConsumption` table. All the data in the column will be lost.
  - You are about to drop the column `folioPaperCount` on the `PaperConsumption` table. All the data in the column will be lost.
  - You are about to drop the column `letterPaperCount` on the `PaperConsumption` table. All the data in the column will be lost.
  - You are about to drop the `PaymentInfo` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Product` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Report` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `Service` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `paperId` to the `PaperConsumption` table without a default value. This is not possible if the table is not empty.
  - Added the required column `quanity` to the `PaperConsumption` table without a default value. This is not possible if the table is not empty.
  - Added the required column `serviceProvided` to the `PaperConsumption` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "PaperCategoryNames" AS ENUM ('BondPaper', 'PhotoPaper', 'StickerPaper', 'TransferPaper', 'SublimationPaper', 'PVCSheet', 'Phototop');

-- DropForeignKey
ALTER TABLE "Report" DROP CONSTRAINT "Report_productId_fkey";

-- DropForeignKey
ALTER TABLE "Report" DROP CONSTRAINT "Report_serviceId_fkey";

-- AlterTable
ALTER TABLE "PaperConsumption" DROP COLUMN "a4PaperCount",
DROP COLUMN "folioPaperCount",
DROP COLUMN "letterPaperCount",
ADD COLUMN     "paperId" INTEGER NOT NULL,
ADD COLUMN     "quanity" INTEGER NOT NULL,
ADD COLUMN     "serviceProvided" "PaperServiceType" NOT NULL,
ALTER COLUMN "paperServiceType" DROP NOT NULL;

-- DropTable
DROP TABLE "PaymentInfo";

-- DropTable
DROP TABLE "Product";

-- DropTable
DROP TABLE "Report";

-- DropTable
DROP TABLE "Service";

-- DropEnum
DROP TYPE "CustomerType";

-- DropEnum
DROP TYPE "PaymentMethod";

-- DropEnum
DROP TYPE "ProductType";

-- DropEnum
DROP TYPE "ServiceType";

-- CreateTable
CREATE TABLE "PaperCategory" (
    "id" SERIAL NOT NULL,
    "categoryName" "PaperCategoryNames" NOT NULL,

    CONSTRAINT "PaperCategory_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PaperBrands" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "PaperBrands_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Paper" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "name" TEXT NOT NULL,
    "paperCategoryId" INTEGER NOT NULL,
    "paperBrandsId" INTEGER NOT NULL,
    "quantityInStocks" INTEGER NOT NULL,
    "length" DOUBLE PRECISION NOT NULL,
    "width" DOUBLE PRECISION NOT NULL,
    "otherSpecs" TEXT,
    "pricePerSheet" DOUBLE PRECISION,

    CONSTRAINT "Paper_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Paper" ADD CONSTRAINT "Paper_paperCategoryId_fkey" FOREIGN KEY ("paperCategoryId") REFERENCES "PaperCategory"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Paper" ADD CONSTRAINT "Paper_paperBrandsId_fkey" FOREIGN KEY ("paperBrandsId") REFERENCES "PaperBrands"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PaperConsumption" ADD CONSTRAINT "PaperConsumption_paperId_fkey" FOREIGN KEY ("paperId") REFERENCES "Paper"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
