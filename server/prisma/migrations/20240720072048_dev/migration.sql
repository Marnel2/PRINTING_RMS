/*
  Warnings:

  - You are about to drop the column `color` on the `Service` table. All the data in the column will be lost.
  - You are about to drop the column `salesId` on the `Service` table. All the data in the column will be lost.
  - You are about to drop the column `serviceCategoryId` on the `Service` table. All the data in the column will be lost.
  - You are about to drop the column `sizeArea` on the `Service` table. All the data in the column will be lost.
  - You are about to drop the column `unitPrice` on the `Service` table. All the data in the column will be lost.
  - You are about to drop the `Sales` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `ServiceCategory` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[email]` on the table `User` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `serviceFee` to the `Service` table without a default value. This is not possible if the table is not empty.
  - Added the required column `serviceName` to the `Service` table without a default value. This is not possible if the table is not empty.
  - Added the required column `serviceType` to the `Service` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "ProductType" AS ENUM ('Paper', 'Film', 'Media', 'PVC', 'Shirt', 'Cap', 'Acrylic');

-- CreateEnum
CREATE TYPE "ServiceType" AS ENUM ('Copy', 'Print', 'Laminate', 'HeatTransfer', 'PrintAndInstall', 'Cutting');

-- CreateEnum
CREATE TYPE "PaymentMethod" AS ENUM ('Cash', 'Gcash', 'BankTransfer', 'Internal');

-- CreateEnum
CREATE TYPE "CustomerType" AS ENUM ('NonMember', 'Member', 'InternalEmployee');

-- DropForeignKey
ALTER TABLE "Sales" DROP CONSTRAINT "Sales_userId_fkey";

-- DropForeignKey
ALTER TABLE "Service" DROP CONSTRAINT "Service_salesId_fkey";

-- DropForeignKey
ALTER TABLE "Service" DROP CONSTRAINT "Service_serviceCategoryId_fkey";

-- AlterTable
ALTER TABLE "Service" DROP COLUMN "color",
DROP COLUMN "salesId",
DROP COLUMN "serviceCategoryId",
DROP COLUMN "sizeArea",
DROP COLUMN "unitPrice",
ADD COLUMN     "serviceFee" DOUBLE PRECISION NOT NULL,
ADD COLUMN     "serviceName" TEXT NOT NULL,
ADD COLUMN     "serviceType" "ServiceType" NOT NULL;

-- DropTable
DROP TABLE "Sales";

-- DropTable
DROP TABLE "ServiceCategory";

-- DropEnum
DROP TYPE "Color";

-- CreateTable
CREATE TABLE "Product" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "productName" TEXT NOT NULL,
    "productType" "ProductType" NOT NULL,
    "quantity" INTEGER NOT NULL,
    "unitPrice" DOUBLE PRECISION NOT NULL,

    CONSTRAINT "Product_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Report" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "serviceId" INTEGER NOT NULL,
    "productId" INTEGER NOT NULL,
    "quantity" INTEGER NOT NULL,
    "totalPrice" DOUBLE PRECISION NOT NULL,
    "remarks" TEXT,

    CONSTRAINT "Report_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "PaymentInfo" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,
    "totalAmount" DOUBLE PRECISION NOT NULL,
    "paymentMethod" "PaymentMethod" NOT NULL,
    "tenderedAmount" DOUBLE PRECISION NOT NULL,
    "changeAmount" DOUBLE PRECISION NOT NULL,
    "transactionNumber" INTEGER,
    "customerName" TEXT,
    "customerType" "CustomerType" NOT NULL,

    CONSTRAINT "PaymentInfo_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");

-- AddForeignKey
ALTER TABLE "Report" ADD CONSTRAINT "Report_serviceId_fkey" FOREIGN KEY ("serviceId") REFERENCES "Service"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Report" ADD CONSTRAINT "Report_productId_fkey" FOREIGN KEY ("productId") REFERENCES "Product"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
