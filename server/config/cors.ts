import { CorsOptions } from "cors";

const WHITELIST = [process.env.CLIENT_HOST_DEV];
export const corsOptions: CorsOptions = {
	origin: (origin, cb) => {
		console.log(`ORIGIN: ${origin}`);
		if (WHITELIST.indexOf(origin) !== -1 || !origin) {
			cb(null, true);
		} else {
			cb(new Error("Not allowed by cors"));
		}
	},
	methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
	credentials: true,
};

