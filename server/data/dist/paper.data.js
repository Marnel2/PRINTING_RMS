"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.getListOfPaperReport = exports.createPaperConsumptionReportData = exports.getPaperByIdAndUpdate = exports.getPaperById = exports.getListOfPaper = exports.createPaper = exports.getBrandByCategory = exports.createPaperBrand = exports.createManyCategory = exports.getCategoryData = exports.createCategory = void 0;
var prisma_service_1 = require("../prisma/prisma.service");
var ErrorHandler_1 = require("../utils/ErrorHandler");
var PaperCategory = prisma_service_1["default"].paperCategory;
var PaperBrand = prisma_service_1["default"].paperBrands;
var Paper = prisma_service_1["default"].paper;
var PaperReport = prisma_service_1["default"].paperConsumption;
exports.createCategory = function (categoryName, next) { return __awaiter(void 0, void 0, void 0, function () {
    var existingCategory, newCategory, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                return [4 /*yield*/, PaperCategory.findFirst({
                        where: {
                            categoryName: categoryName
                        }
                    })];
            case 1:
                existingCategory = _a.sent();
                if (existingCategory) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("The category already exists.", 400))];
                }
                return [4 /*yield*/, PaperCategory.create({
                        data: {
                            categoryName: categoryName
                        }
                    })];
            case 2:
                newCategory = _a.sent();
                if (!newCategory) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Something went wrong, Please try again!", 500))];
                }
                return [2 /*return*/, newCategory];
            case 3:
                error_1 = _a.sent();
                return [2 /*return*/, next(new ErrorHandler_1["default"](error_1.message, 500))];
            case 4: return [2 /*return*/];
        }
    });
}); };
exports.getCategoryData = function (next) { return __awaiter(void 0, void 0, void 0, function () {
    var categories, error_2;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, PaperCategory.findMany({})];
            case 1:
                categories = _a.sent();
                if (!categories) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Something went wrong.", 500))];
                }
                return [2 /*return*/, categories];
            case 2:
                error_2 = _a.sent();
                return [2 /*return*/, next(new ErrorHandler_1["default"](error_2.message, 500))];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.createManyCategory = function (next) { return __awaiter(void 0, void 0, void 0, function () {
    var newCategories, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, PaperCategory.createMany({
                        data: [
                            {
                                categoryName: "StickerPaper"
                            },
                            {
                                categoryName: "Phototop"
                            },
                            {
                                categoryName: "TransferPaper"
                            },
                            {
                                categoryName: "SublimationPaper"
                            },
                        ]
                    })];
            case 1:
                newCategories = _a.sent();
                return [2 /*return*/, newCategories];
            case 2:
                error_3 = _a.sent();
                return [2 /*return*/, next(new ErrorHandler_1["default"](error_3.message, 500))];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.createPaperBrand = function (_a, next) {
    var brandName = _a.brandName, parsedCategory = _a.parsedCategory;
    return __awaiter(void 0, void 0, void 0, function () {
        var newBrand, error_4;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, PaperBrand.create({
                            data: {
                                name: brandName,
                                categoryId: parsedCategory
                            }
                        })];
                case 1:
                    newBrand = _b.sent();
                    return [2 /*return*/, newBrand];
                case 2:
                    error_4 = _b.sent();
                    return [2 /*return*/, next(new ErrorHandler_1["default"](error_4.message, 500))];
                case 3: return [2 /*return*/];
            }
        });
    });
};
exports.getBrandByCategory = function (categoryId, next) { return __awaiter(void 0, void 0, void 0, function () {
    var brand, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, PaperBrand.findMany({
                        where: {
                            categoryId: categoryId
                        }
                    })];
            case 1:
                brand = _a.sent();
                return [2 /*return*/, brand];
            case 2:
                error_5 = _a.sent();
                return [2 /*return*/, next(new ErrorHandler_1["default"](error_5.message, 500))];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.createPaper = function (_a, next) {
    var name = _a.name, category = _a.category, brand = _a.brand, quantityInStocks = _a.quantityInStocks, length = _a.length, width = _a.width, otherSpecs = _a.otherSpecs, pricePerSheet = _a.pricePerSheet;
    return __awaiter(void 0, void 0, void 0, function () {
        var paperData, paper, error_6;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 2, , 3]);
                    paperData = {
                        name: name,
                        paperCategoryId: parseInt(category),
                        paperBrandsId: parseInt(brand),
                        quantityInStocks: quantityInStocks,
                        length: length,
                        width: width,
                        otherSpecs: otherSpecs,
                        pricePerSheet: pricePerSheet
                    };
                    return [4 /*yield*/, Paper.create({
                            data: paperData
                        })];
                case 1:
                    paper = _b.sent();
                    return [2 /*return*/, paper];
                case 2:
                    error_6 = _b.sent();
                    return [2 /*return*/, next(new ErrorHandler_1["default"](error_6.message, 500))];
                case 3: return [2 /*return*/];
            }
        });
    });
};
exports.getListOfPaper = function (_a, next) {
    var parsedPaperBrand = _a.parsedPaperBrand, parsedPaperCategory = _a.parsedPaperCategory;
    return __awaiter(void 0, void 0, void 0, function () {
        var papers, error_7;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, Paper.findMany({
                            where: {
                                paperCategoryId: parsedPaperCategory,
                                paperBrandsId: parsedPaperBrand
                            }
                        })];
                case 1:
                    papers = _b.sent();
                    return [2 /*return*/, papers];
                case 2:
                    error_7 = _b.sent();
                    return [2 /*return*/, next(new ErrorHandler_1["default"](error_7.message, 500))];
                case 3: return [2 /*return*/];
            }
        });
    });
};
exports.getPaperById = function (paperId, next) { return __awaiter(void 0, void 0, void 0, function () {
    var paper, error_8;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, Paper.findUnique({
                        where: {
                            id: paperId
                        }
                    })];
            case 1:
                paper = _a.sent();
                return [2 /*return*/, paper];
            case 2:
                error_8 = _a.sent();
                return [2 /*return*/, next(new ErrorHandler_1["default"](error_8.message, 500))];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.getPaperByIdAndUpdate = function (_a, next) {
    var parsedPaperId = _a.parsedPaperId, parsedQty = _a.parsedQty;
    return __awaiter(void 0, void 0, void 0, function () {
        var updatePaper, error_9;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 2, , 3]);
                    return [4 /*yield*/, Paper.update({
                            where: {
                                id: parsedPaperId
                            },
                            data: {
                                quantityInStocks: {
                                    decrement: parsedQty
                                }
                            }
                        })];
                case 1:
                    updatePaper = _b.sent();
                    if (!updatePaper) {
                        return [2 /*return*/, next(new ErrorHandler_1["default"]("Something went wrong", 500))];
                    }
                    return [2 /*return*/, updatePaper];
                case 2:
                    error_9 = _b.sent();
                    return [2 /*return*/, next(new ErrorHandler_1["default"](error_9.message, 500))];
                case 3: return [2 /*return*/];
            }
        });
    });
};
exports.createPaperConsumptionReportData = function (reportData, next) { return __awaiter(void 0, void 0, void 0, function () {
    var singleReport, error_10;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, PaperReport.create({
                        data: reportData
                    })];
            case 1:
                singleReport = _a.sent();
                return [2 /*return*/, singleReport];
            case 2:
                error_10 = _a.sent();
                return [2 /*return*/, next(new ErrorHandler_1["default"](error_10.message, 500))];
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.getListOfPaperReport = function (next) { return __awaiter(void 0, void 0, void 0, function () {
    var paperReports, error_11;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, PaperReport.findMany({
                        orderBy: {
                            createdAt: "desc"
                        },
                        include: {
                            product: {
                                select: {
                                    brand: {
                                        select: {
                                            name: true
                                        }
                                    },
                                    name: true
                                }
                            }
                        }
                    })];
            case 1:
                paperReports = _a.sent();
                return [2 /*return*/, paperReports];
            case 2:
                error_11 = _a.sent();
                return [2 /*return*/, next(new ErrorHandler_1["default"](error_11.message, 500))];
            case 3: return [2 /*return*/];
        }
    });
}); };
