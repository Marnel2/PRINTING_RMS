import prisma from "../prisma/prisma.service";
import ErrorHandler from "../utils/ErrorHandler";
import { NextFunction, Response } from "express";

const User = prisma.user;

// REGISTER USER
interface IRegistrationBody {
	name: string;
	email: string;
	password: string;
}

export const createNewUser = async (
	{ name, email, password }: IRegistrationBody,
	next: NextFunction
) => {
	if (!name || !email || !password) {
		return next(
			new ErrorHandler("Please provide all required informations.", 400)
		);
	}

	const data: IRegistrationBody = {
		name,
		email,
		password,
	};

	const newUser = await User.create({
		data,
	});

	if (!newUser) {
		return next(
			new ErrorHandler("Something went wrong, Please try again.", 500)
		);
	}

	return newUser;
};

export const findUserById = async (
	userId: number,
	returnPassword: boolean,
	next: NextFunction
) => {
	if (!userId) return next(new ErrorHandler("Invalid user ID", 400));

	const user = await User.findUnique({
		where: { id: userId },
		select: {
			id: true,
			name: true,
			email: true,
			password: returnPassword,
		},
	});

	if (!user) return next(new ErrorHandler("User does not exist", 400));

	return user;
};

export const findUserByEmail = async (
	email: string,
	returnPassword: boolean,
	next: NextFunction
) => {
	if (!email || typeof email !== "string") {
		return next(new ErrorHandler("Invalid email", 400));
	}

	const user = await User.findUnique({
		where: { email },
		select: {
			id: true,
			name: true,
			email: true,
			password: returnPassword,
		},
	});

	return user;
};
