import { IPaperBody } from "../@types";
import prisma from "../prisma/prisma.service";
import ErrorHandler from "../utils/ErrorHandler";
import { NextFunction } from "express";

import { PaperCategoryNames, PaperServiceType } from "@prisma/client";

const PaperCategory = prisma.paperCategory;
const PaperBrand = prisma.paperBrands;
const Paper = prisma.paper;
const PaperReport = prisma.paperConsumption;

export const createCategory = async (
	categoryName: PaperCategoryNames,
	next: NextFunction
) => {
	try {
		const existingCategory = await PaperCategory.findFirst({
			where: {
				categoryName,
			},
		});

		if (existingCategory) {
			return next(new ErrorHandler("The category already exists.", 400));
		}

		const newCategory = await PaperCategory.create({
			data: {
				categoryName,
			},
		});

		if (!newCategory) {
			return next(
				new ErrorHandler("Something went wrong, Please try again!", 500)
			);
		}
		return newCategory;
	} catch (error: any) {
		return next(new ErrorHandler(error.message, 500));
	}
};

export const getCategoryData = async (next: NextFunction) => {
	try {
		const categories = await PaperCategory.findMany({});

		if (!categories) {
			return next(new ErrorHandler("Something went wrong.", 500));
		}

		return categories;
	} catch (error: any) {
		return next(new ErrorHandler(error.message, 500));
	}
};

export const createManyCategory = async (next: NextFunction) => {
	try {
		const newCategories = await PaperCategory.createMany({
			data: [
				{
					categoryName: "StickerPaper",
				},
				{
					categoryName: "Phototop",
				},
				{
					categoryName: "TransferPaper",
				},
				{
					categoryName: "SublimationPaper",
				},
			],
		});

		return newCategories;
	} catch (error: any) {
		return next(new ErrorHandler(error.message, 500));
	}
};

interface ICreateBrandParams {
	brandName: string;
	parsedCategory: number;
}

export const createPaperBrand = async (
	{ brandName, parsedCategory }: ICreateBrandParams,
	next: NextFunction
) => {
	try {
		const newBrand = await PaperBrand.create({
			data: {
				name: brandName,
				categoryId: parsedCategory,
			},
		});

		return newBrand;
	} catch (error: any) {
		return next(new ErrorHandler(error.message, 500));
	}
};

export const getBrandByCategory = async (
	categoryId: number,
	next: NextFunction
) => {
	try {
		const brand = await PaperBrand.findMany({
			where: {
				categoryId,
			},
		});

		return brand;
	} catch (error: any) {
		return next(new ErrorHandler(error.message, 500));
	}
};

export const createPaper = async (
	{
		name,
		category,
		brand,
		quantityInStocks,
		length,
		width,
		otherSpecs,
		pricePerSheet,
	}: IPaperBody,
	next: NextFunction
) => {
	try {
		const paperData = {
			name,
			paperCategoryId: parseInt(category),
			paperBrandsId: parseInt(brand),
			quantityInStocks,
			length,
			width,
			otherSpecs,
			pricePerSheet,
		};

		const paper = await Paper.create({
			data: paperData,
		});

		return paper;
	} catch (error: any) {
		return next(new ErrorHandler(error.message, 500));
	}
};

type ListOfPaperParams = {
	parsedPaperBrand: number;
	parsedPaperCategory: number;
};

export const getListOfPaper = async (
	{ parsedPaperBrand, parsedPaperCategory }: ListOfPaperParams,
	next: NextFunction
) => {
	try {
		const papers = await Paper.findMany({
			where: {
				paperCategoryId: parsedPaperCategory,
				paperBrandsId: parsedPaperBrand,
			},
		});

		return papers;
	} catch (error: any) {
		return next(new ErrorHandler(error.message, 500));
	}
};

export const getPaperById = async (paperId: number, next: NextFunction) => {
	try {
		const paper = await Paper.findUnique({
			where: {
				id: paperId,
			},
		});

		return paper;
	} catch (error: any) {
		return next(new ErrorHandler(error.message, 500));
	}
};

type getPaperByIdAndUpdateParams = {
	parsedPaperId: number;
	parsedQty: number;
};

export const getPaperByIdAndUpdate = async (
	{ parsedPaperId, parsedQty }: getPaperByIdAndUpdateParams,
	next: NextFunction
) => {
	try {
		const updatePaper = await Paper.update({
			where: {
				id: parsedPaperId,
			},
			data: {
				quantityInStocks: {
					decrement: parsedQty,
				},
			},
		});

		if (!updatePaper) {
			return next(new ErrorHandler("Something went wrong", 500));
		}

		return updatePaper;
	} catch (error: any) {
		return next(new ErrorHandler(error.message, 500));
	}
};

export type createPaperReportParams = {
	paperId: number;
	quantity: number;
	serviceProvided: PaperServiceType;
	remarks: string;
	createdAt: Date;
};

export const createPaperConsumptionReportData = async (
	reportData: createPaperReportParams,
	next: NextFunction
) => {
	try {
		const singleReport = await PaperReport.create({
			data: reportData,
		});

		return singleReport;
	} catch (error: any) {
		return next(new ErrorHandler(error.message, 500));
	}
};

export const getListOfPaperReport = async (next: NextFunction) => {
	try {
		const paperReports = await PaperReport.findMany({
			orderBy: {
				createdAt: "desc",
			},
			include: {
				product: {
					select: {
						brand: {
							select: {
								name: true,
							},
						},
						name: true,
					},
				},
			},
		});

		return paperReports;
	} catch (error: any) {
		return next(new ErrorHandler(error.message, 500));
	}
};
