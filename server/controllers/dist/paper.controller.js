"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.getPaperReport = exports.createPaperConsumptionReport = exports.getPapers = exports.addNewPaper = exports.getPaperBrands = exports.addPaperBrand = exports.getCategories = exports.addPaperCategory = void 0;
var catchAsyncErrors_1 = require("../middlewares/catchAsyncErrors");
var ErrorHandler_1 = require("../utils/ErrorHandler");
var paper_data_1 = require("../data/paper.data");
exports.addPaperCategory = catchAsyncErrors_1.CatchAsyncError(function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var categoryName, newPaperCategory;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                categoryName = req.body.categoryName;
                if (!categoryName) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Please provide category name", 400))];
                }
                return [4 /*yield*/, paper_data_1.createCategory(categoryName, next)];
            case 1:
                newPaperCategory = _a.sent();
                if (!newPaperCategory)
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Something went wrong, Please try again!", 500))];
                res.status(200).json({
                    success: true,
                    message: "New Category " + categoryName + " added!"
                });
                return [2 /*return*/];
        }
    });
}); });
exports.getCategories = catchAsyncErrors_1.CatchAsyncError(function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var categories;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, paper_data_1.getCategoryData(next)];
            case 1:
                categories = _a.sent();
                res.status(200).json({
                    success: true,
                    categories: categories
                });
                return [2 /*return*/];
        }
    });
}); });
exports.addPaperBrand = catchAsyncErrors_1.CatchAsyncError(function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, brandName, category, parsedCategory, newPaperBrand;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.body, brandName = _a.brandName, category = _a.category;
                if (!brandName || !category) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Please provide all necessary informations", 400))];
                }
                parsedCategory = parseInt(category);
                return [4 /*yield*/, paper_data_1.createPaperBrand({ brandName: brandName, parsedCategory: parsedCategory }, next)];
            case 1:
                newPaperBrand = _b.sent();
                if (!newPaperBrand) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Something went wrong, please try again later.", 500))];
                }
                res.status(201).json({
                    success: true,
                    message: "New brand has been added"
                });
                return [2 /*return*/];
        }
    });
}); });
exports.getPaperBrands = catchAsyncErrors_1.CatchAsyncError(function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var categoryId, parsedCategoryId, brands;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                categoryId = req.params.categoryId;
                if (!categoryId) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Invalid category id", 400))];
                }
                parsedCategoryId = parseInt(categoryId);
                return [4 /*yield*/, paper_data_1.getBrandByCategory(parsedCategoryId, next)];
            case 1:
                brands = _a.sent();
                if (!brands) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Something went wrong.", 500))];
                }
                res.status(200).json({
                    success: true,
                    brands: brands
                });
                return [2 /*return*/];
        }
    });
}); });
exports.addNewPaper = catchAsyncErrors_1.CatchAsyncError(function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, category, name, brand, quantity, length, width, otherSpecs, pricePerSheet, paperData, paper;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.body, category = _a.category, name = _a.name, brand = _a.brand, quantity = _a.quantity, length = _a.length, width = _a.width, otherSpecs = _a.otherSpecs, pricePerSheet = _a.pricePerSheet;
                if (!name || !category || !brand || !quantity || !length || !width) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Please fill in required page", 400))];
                }
                paperData = {
                    name: name,
                    category: category,
                    brand: brand,
                    quantityInStocks: parseInt(quantity),
                    length: parseFloat(length),
                    width: parseFloat(width),
                    otherSpecs: otherSpecs,
                    pricePerSheet: parseFloat(pricePerSheet)
                };
                return [4 /*yield*/, paper_data_1.createPaper(paperData, next)];
            case 1:
                paper = _b.sent();
                if (!paper) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Something went wrong", 500))];
                }
                res.status(201).json({
                    sucess: true,
                    paper: paper
                });
                return [2 /*return*/];
        }
    });
}); });
exports.getPapers = catchAsyncErrors_1.CatchAsyncError(function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, paperBrand, paperCategory, parsedPaperBrand, parsedPaperCategory, papers;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.body, paperBrand = _a.paperBrand, paperCategory = _a.paperCategory;
                if (!paperBrand || !paperCategory) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Invalid input.", 400))];
                }
                parsedPaperBrand = parseInt(paperBrand);
                parsedPaperCategory = parseInt(paperCategory);
                return [4 /*yield*/, paper_data_1.getListOfPaper({ parsedPaperBrand: parsedPaperBrand, parsedPaperCategory: parsedPaperCategory }, next)];
            case 1:
                papers = _b.sent();
                if (!papers) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Something went wrong.", 400))];
                }
                res.status(200).json({
                    success: true,
                    papers: papers
                });
                return [2 /*return*/];
        }
    });
}); });
exports.createPaperConsumptionReport = catchAsyncErrors_1.CatchAsyncError(function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, serviceProvided, quantity, remarks, paperId, createdAt, parsedPaperId, paper, parsedQty, updatePaper, reportData, report;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.body, serviceProvided = _a.serviceProvided, quantity = _a.quantity, remarks = _a.remarks, paperId = _a.paperId, createdAt = _a.createdAt;
                if (!serviceProvided || !quantity || !paperId) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Invalid input", 400))];
                }
                parsedPaperId = parseInt(paperId);
                return [4 /*yield*/, paper_data_1.getPaperById(parsedPaperId, next)];
            case 1:
                paper = _b.sent();
                if ((paper === null || paper === void 0 ? void 0 : paper.quantityInStocks) && quantity > paper.quantityInStocks) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Insufficient amount of paper", 400))];
                }
                parsedQty = parseInt(quantity);
                return [4 /*yield*/, paper_data_1.getPaperByIdAndUpdate({ parsedPaperId: parsedPaperId, parsedQty: parsedQty }, next)];
            case 2:
                updatePaper = _b.sent();
                if (!updatePaper) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Could not update paper data", 500))];
                }
                reportData = {
                    serviceProvided: serviceProvided,
                    quantity: parsedQty,
                    remarks: remarks,
                    paperId: parsedPaperId,
                    createdAt: createdAt
                };
                return [4 /*yield*/, paper_data_1.createPaperConsumptionReportData(reportData, next)];
            case 3:
                report = _b.sent();
                if (!report) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Could not create report", 500))];
                }
                res.status(201).json({
                    success: true,
                    message: "Successfuly Created!"
                });
                return [2 /*return*/];
        }
    });
}); });
exports.getPaperReport = catchAsyncErrors_1.CatchAsyncError(function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var paperReports;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, paper_data_1.getListOfPaperReport(next)];
            case 1:
                paperReports = _a.sent();
                if (!paperReports) {
                    return [2 /*return*/, next(new ErrorHandler_1["default"]("Something went wrong", 500))];
                }
                res.status(200).json({
                    success: true,
                    paperReports: paperReports
                });
                return [2 /*return*/];
        }
    });
}); });
