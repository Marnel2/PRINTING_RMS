import { Request, Response, NextFunction } from "express";
import { CatchAsyncError } from "../middlewares/catchAsyncErrors";
import ErrorHandler from "../utils/ErrorHandler";

export const addService = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		const { serviceName, serviceType, serviceFee } = req.body;
		try {
			if (!serviceType || !serviceName) {
				return next(
					new ErrorHandler("Please provice required informations", 400)
				);
			}
		} catch (error: any) {
			return next(new ErrorHandler(error.message, 500));
		}
	}
);
