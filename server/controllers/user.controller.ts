import { Request, Response, NextFunction } from "express";
import { CatchAsyncError } from "../middlewares/catchAsyncErrors";
import ErrorHandler from "../utils/ErrorHandler";
import { comparePassword } from "../services/auth.service";
import {
	accessTokenOptions,
	refreshTokenOptions,
	sendToken,
} from "../utils/jwt";
import jwt, { JwtPayload } from "jsonwebtoken";
import {
	createNewUser,
	findUserByEmail,
	findUserById,
} from "../data/user.data";

export const createUser = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		try {
			const { name, email, password } = req.body;

			const userExists = await findUserByEmail(email, true, next);

			if (userExists) {
				return next(new ErrorHandler("Email is already taken.", 400));
			}

			const newUser = await createNewUser({ name, email, password }, next);

			res.status(201).json({
				success: true,
				message: "New user has been succesfully registered an account.",
				newUser,
			});
		} catch (error: any) {
			return next(new ErrorHandler(error.message, 500));
		}
	}
);

// LOGIN USER
export const loginUser = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		try {
			const { email, password } = req.body;

			const user = await findUserByEmail(email, true, next);

			if (!user) return next(new ErrorHandler("Incorrect email", 400));

			const isPasswordMatch = await comparePassword(password, user?.password);

			if (!isPasswordMatch)
				return next(new ErrorHandler("Incorrect password", 400));

			sendToken(user, 200, res);
		} catch (error: any) {
			return next(new ErrorHandler(error.message, 500));
		}
	}
);

// FETCH CURRENT USER
export const fetchUser = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		try {
			const { access_token } = req.cookies;

			if (!access_token) {
				return next(new ErrorHandler("Access token is missing", 401));
			}

			const decoded = jwt.verify(
				access_token,
				process.env.ACCESS_TOKEN_SECRET as string
			) as JwtPayload;

			if (!decoded) return next(new ErrorHandler("Token is invalid", 401));

			const user = await findUserById(decoded.id, false, next);

			if (!user) {
				return next(new ErrorHandler("User not found", 401));
			}

			res.status(200).json({
				success: true,
				user,
			});
		} catch (error: any) {
			return next(new ErrorHandler(error.message, 401));
		}
	}
);

// LOGOUT USER
export const logout = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		try {
			res.clearCookie("access_token");
			res.clearCookie("refresh_token");

			console.log("LOGOUT");

			res.status(200).json({
				success: true,
				message: "Logged out succesfully",
			});
		} catch (error: any) {
			return next(new ErrorHandler(error.message, 500));
		}
	}
);

// Refresh token
export const refreshToken = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		try {
			const refresh_token = req.cookies.refresh_token as string;

			if (!refresh_token) {
				return next(new ErrorHandler("Refresh token is missing", 401));
			}

			console.log("HERE");

			let decoded: JwtPayload;

			try {
				decoded = jwt.verify(
					refresh_token,
					process.env.REFRESH_TOKEN_SECRET as string
				) as JwtPayload;
			} catch (error) {
				return next(new ErrorHandler("Invalid or expired refresh token", 401));
			}

			let message = "Could not refresh token.";

			if (!decoded) return next(new ErrorHandler(message, 400));

			const userSession = await findUserById(decoded.id, false, next);

			if (!userSession) {
				return next(
					new ErrorHandler("Please login to access this resource", 400)
				);
			}

			if (!userSession) {
				return next(new ErrorHandler("User not found", 404));
			}

			const accessToken = jwt.sign(
				{ id: userSession.id },
				process.env.ACCESS_TOKEN_SECRET as string,
				{
					expiresIn: "5m",
				}
			);

			const refreshToken = jwt.sign(
				{ id: userSession.id },
				process.env.REFRESH_TOKEN_SECRET as string,
				{
					expiresIn: "3d",
				}
			);

			req.user = userSession;

			res.cookie("access_token", accessToken, accessTokenOptions);
			res.cookie("refresh_token", refreshToken, refreshTokenOptions);

			console.log("REFRESHED");

			res.status(200).json({
				success: true,
			});
		} catch (error: any) {
			return next(new ErrorHandler(error.message, 500));
		}
	}
);
