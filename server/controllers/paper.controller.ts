import { Request, Response, NextFunction } from "express";
import { CatchAsyncError } from "../middlewares/catchAsyncErrors";
import ErrorHandler from "../utils/ErrorHandler";
import {
	createCategory,
	createPaper,
	createPaperBrand,
	createPaperConsumptionReportData,
	getBrandByCategory,
	getCategoryData,
	getListOfPaper,
	getListOfPaperReport,
	getPaperById,
	getPaperByIdAndUpdate,
} from "../data/paper.data";
import { IPaperBody } from "../@types";

export const addPaperCategory = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		const { categoryName } = req.body;

		if (!categoryName) {
			return next(new ErrorHandler("Please provide category name", 400));
		}

		const newPaperCategory = await createCategory(categoryName, next);

		if (!newPaperCategory)
			return next(
				new ErrorHandler("Something went wrong, Please try again!", 500)
			);

		res.status(200).json({
			success: true,
			message: `New Category ${categoryName} added!`,
		});
	}
);

export const getCategories = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		const categories = await getCategoryData(next);

		res.status(200).json({
			success: true,
			categories,
		});
	}
);

export const addPaperBrand = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		const { brandName, category } = req.body;

		if (!brandName || !category) {
			return next(
				new ErrorHandler("Please provide all necessary informations", 400)
			);
		}

		const parsedCategory = parseInt(category);

		const newPaperBrand = await createPaperBrand(
			{ brandName, parsedCategory },
			next
		);

		if (!newPaperBrand) {
			return next(
				new ErrorHandler("Something went wrong, please try again later.", 500)
			);
		}

		res.status(201).json({
			success: true,
			message: "New brand has been added",
		});
	}
);

export const getPaperBrands = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		const { categoryId } = req.params;

		if (!categoryId) {
			return next(new ErrorHandler("Invalid category id", 400));
		}

		const parsedCategoryId = parseInt(categoryId);

		const brands = await getBrandByCategory(parsedCategoryId, next);

		if (!brands) {
			return next(new ErrorHandler("Something went wrong.", 500));
		}

		res.status(200).json({
			success: true,
			brands,
		});
	}
);

export const addNewPaper = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		const {
			category,
			name,
			brand,
			quantity,
			length,
			width,
			otherSpecs,
			pricePerSheet,
		} = req.body;

		if (!name || !category || !brand || !quantity || !length || !width) {
			return next(new ErrorHandler("Please fill in required page", 400));
		}

		const paperData: IPaperBody = {
			name,
			category,
			brand,
			quantityInStocks: parseInt(quantity),
			length: parseFloat(length),
			width: parseFloat(width),
			otherSpecs,
			pricePerSheet: parseFloat(pricePerSheet),
		};

		const paper = await createPaper(paperData, next);

		if (!paper) {
			return next(new ErrorHandler("Something went wrong", 500));
		}

		res.status(201).json({
			sucess: true,
			paper,
		});
	}
);

export const getPapers = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		const { paperBrand, paperCategory } = req.body;

		if (!paperBrand || !paperCategory) {
			return next(new ErrorHandler("Invalid input.", 400));
		}

		const parsedPaperBrand = parseInt(paperBrand);
		const parsedPaperCategory = parseInt(paperCategory);

		const papers = await getListOfPaper(
			{ parsedPaperBrand, parsedPaperCategory },
			next
		);

		if (!papers) {
			return next(new ErrorHandler("Something went wrong.", 400));
		}

		res.status(200).json({
			success: true,
			papers,
		});
	}
);

export const createPaperConsumptionReport = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		const { serviceProvided, quantity, remarks, paperId, createdAt } = req.body;

		if (!serviceProvided || !quantity || !paperId) {
			return next(new ErrorHandler("Invalid input", 400));
		}

		const parsedPaperId = parseInt(paperId);

		const paper = await getPaperById(parsedPaperId, next);

		if (paper?.quantityInStocks && quantity > paper.quantityInStocks) {
			return next(new ErrorHandler("Insufficient amount of paper", 400));
		}

		const parsedQty = parseInt(quantity);

		const updatePaper = await getPaperByIdAndUpdate(
			{ parsedPaperId, parsedQty },
			next
		);

		if (!updatePaper) {
			return next(new ErrorHandler("Could not update paper data", 500));
		}

		const reportData = {
			serviceProvided,
			quantity: parsedQty,
			remarks,
			paperId: parsedPaperId,
			createdAt,
		};

		const report = await createPaperConsumptionReportData(reportData, next);

		if (!report) {
			return next(new ErrorHandler("Could not create report", 500));
		}

		res.status(201).json({
			success: true,
			message: "Successfuly Created!",
		});
	}
);

export const getPaperReport = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		const paperReports = await getListOfPaperReport(next);

		if (!paperReports) {
			return next(new ErrorHandler("Something went wrong", 500));
		}

		res.status(200).json({
			success: true,
			paperReports,
		});
	}
);
