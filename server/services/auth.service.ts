import bcrypt from "bcryptjs";

export const comparePassword = async (
	enteredPassword: string,
	userPassword: string
) => {
	return await bcrypt.compare(enteredPassword, userPassword);
};
