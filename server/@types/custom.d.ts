import { Request } from "express";
import { IUser } from "../utils/jwt";

declare global {
	namespace Express {
		interface Request {
			user: IUser;
		}
	}
}
