export interface IPaperBody {
	name: string;
	category: string;
	brand: string;
	quantityInStocks: number;
	length: number;
	width: number;
	otherSpecs: string;
	pricePerSheet: number;
}
