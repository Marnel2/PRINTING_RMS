import jwt from "jsonwebtoken";
import { Response } from "express";

const accessTokenExpires = parseInt(
	process.env.ACCESS_TOKEN_EXPIRE || "300",
	10
);

const refreshTokenExpires = parseInt(
	process.env.REFRESH_TOKEN_EXPIRE || "1200",
	10
);

interface ITokenOptions {
	expires: Date;
	maxAge: number;
	httpOnly: boolean;
	sameSite: "lax" | "strict" | "none" | undefined;
	secure?: boolean;
}

// Options for cookies
export const accessTokenOptions: ITokenOptions = {
	expires: new Date(Date.now() + accessTokenExpires * 60 * 60 * 1000),
	maxAge: accessTokenExpires * 60 * 60 * 1000,
	httpOnly: true,
	sameSite: "strict",
};

export const refreshTokenOptions: ITokenOptions = {
	expires: new Date(Date.now() + refreshTokenExpires * 24 * 60 * 60 * 1000),
	maxAge: refreshTokenExpires * 24 * 60 * 60 * 1000,
	httpOnly: true,
	sameSite: "strict",
};

const signAccessToken = function (userId: number) {
	return jwt.sign({ id: userId }, process.env.ACCESS_TOKEN_SECRET || "", {
		expiresIn: "5m",
	});
};

const signRefreshToken = function (userId: number) {
	return jwt.sign({ id: userId }, process.env.REFRESH_TOKEN_SECRET || "", {
		expiresIn: "3d",
	});
};

export interface IUser {
	id: number;
	name: string;
	email: string;
	password: string;
}

export const sendToken = (user: IUser, statusCode: number, res: Response) => {
	const accessToken = signAccessToken(user?.id);
	const refreshToken = signRefreshToken(user?.id);

	if (process.env.NODE_ENV === "production") accessTokenOptions.secure = true;

	res.cookie("access_token", accessToken, accessTokenOptions);
	res.cookie("refresh_token", refreshToken, refreshTokenOptions);

	const userData = {
		name: user.name,
		email: user.email,
	};

	res.status(statusCode).json({
		success: true,
		userData,
	});
};
