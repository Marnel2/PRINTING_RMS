import bcrypt from "bcryptjs";
import { Prisma } from "@prisma/client";

const hashPassword: Prisma.Middleware = async (
	params: Prisma.MiddlewareParams,
	next
) => {
	if (
		params.model === "User" &&
		(params.action === "create" || params.action === "update")
	) {
		const { data } = params.args;
		if (data.password) {
			const hashedPassword = await bcrypt.hash(data.password, 10);

			data.password = hashedPassword;
		}
	}

	return next(params);
};

export default hashPassword;
