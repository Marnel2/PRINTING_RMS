import { Request, Response, NextFunction } from "express";
import { CatchAsyncError } from "./catchAsyncErrors";
import ErrorHandler from "../utils/ErrorHandler";
import jwt, { JwtPayload } from "jsonwebtoken";

import prisma from "../prisma/prisma.service";

// Authenticated users
export const isAuthenticated = CatchAsyncError(
	async (req: Request, res: Response, next: NextFunction) => {
		const accessToken = req.cookies.access_token as string;

		if (!accessToken) {
			return next(
				new ErrorHandler("Please login to access this resource", 401)
			);
		}

		const decoded = jwt.verify(
			accessToken,
			process.env.ACCESS_TOKEN_SECRET as string
		) as JwtPayload;

		if (!decoded) {
			return next(
				new ErrorHandler("Access token is not valid. Please try again.", 401)
			);
		}

		const user = await prisma.user.findUnique({
			where: {
				id: decoded.id,
			},
		});

		if (!user) {
			return next(
				new ErrorHandler("Please login to access this resource", 401)
			);
		}

		req.user = user;

		next();
	}
);

// // Validate user roles
// export const authorizeRoles = (...roles: string[]) => {
// 	return (req: Request, res: Response, next: NextFunction) => {
// 		if (!roles.includes(req.user.role)) {
// 			return next(
// 				new ErrorHandler(
// 					`Role: ${req.user?.role} is not allowed to access this resource`,
// 					404
// 				)
// 			);
// 		}

// 		next();
// 	};
// };
