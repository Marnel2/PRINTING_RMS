import { NextFunction, Request, Response } from "express";

type AsyncMiddlewareHandler = (
	req: Request,
	res: Response,
	next: NextFunction
) => Promise<void>;

export const CatchAsyncError =
	(theFunc: AsyncMiddlewareHandler) =>
	(req: Request, res: Response, next: NextFunction) => {
		Promise.resolve(theFunc(req, res, next).catch(next));
	};
