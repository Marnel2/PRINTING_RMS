import { useEffect } from "react";
import useAuthStore from "@/store/authStore";

export const useAuth = () => {
	const { user, fetchUser } = useAuthStore();

	useEffect(() => {
		if (!user) {
			fetchUser();
		}
	}, [user, fetchUser]);

	return useAuthStore();
};
