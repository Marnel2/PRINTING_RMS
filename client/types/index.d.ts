declare interface HeaderBoxProps {
	type?: "title" | "greeting";
	title: string;
	subtext: string;
	user?: string;
}

declare interface User {
	id: string;
	email: string;
	name: string;
}
