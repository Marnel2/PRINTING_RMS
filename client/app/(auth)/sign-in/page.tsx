"use client";
import React, { useEffect, useState } from "react";
import { FaRegEyeSlash, FaRegEye } from "react-icons/fa6";
import { useRouter } from "next/navigation";
import useAuthStore from "@/store/authStore";
import { useToast } from "@/components/ui/use-toast";

const Signin = () => {
	const [showPassword, setShowPassword] = useState(false);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const { toast } = useToast();

	const router = useRouter();

	const { login, isLoginSucess, error } = useAuthStore();

	const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();

		if (email && password) {
			await login(email, password);
		}
	};

	useEffect(() => {
		if (isLoginSucess) {
			router.push("/");
			toast({
				title: "Welcome, User!",
				className: "success-toast",
			});
		}
		if (error) {
			toast({
				title: "Error",
				description: error,
				className: "error-toast",
			});
		}
	}, [isLoginSucess, error]);

	return (
		<section className="flex items-center justify-center w-full h-[100vh] bg-bg-image bg-cover bg-no-repeat bg-center">
			<div className="p-5 backdrop-blur-md bg-white/30 rounded-md shadow-md min-w-[350px]">
				<h1 className="text-center font-bold text-[25px] text-white">
					SIGN IN HERE
				</h1>
				<form onSubmit={handleSubmit}>
					<div className="form-group">
						<input
							type="email"
							className="input"
							placeholder="Email"
							name="email"
							onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
								setEmail(e.target.value)
							}
						/>
					</div>
					<div className="form-group">
						<div className="relative">
							<input
								type={showPassword ? "text" : "password"}
								className="input"
								placeholder="Password"
								name="password"
								onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
									setPassword(e.target.value)
								}
							/>
							<button
								type="button"
								className="absolute bottom-3 right-3 text-[#555]"
								onClick={() => setShowPassword(!showPassword)}
							>
								{showPassword ? <FaRegEyeSlash /> : <FaRegEye />}
							</button>
						</div>
					</div>
					<div className="form-group">
						<button
							type="submit"
							className="bg-primary w-full p-2 text-white font-medium rounded-md transition-all "
						>
							SIGN IN
						</button>
					</div>
				</form>
			</div>
		</section>
	);
};

export default Signin;
