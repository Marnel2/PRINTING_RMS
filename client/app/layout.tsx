import type { Metadata } from "next";
import { IBM_Plex_Serif, Orbitron, Rubik, Roboto } from "next/font/google";
import "./globals.css";
import { cn } from "@/lib/utils";
import { Toaster } from "@/components/ui/toaster";

const ibmPlex = IBM_Plex_Serif({
	subsets: ["latin"],
	weight: ["400", "700"],
	variable: "--font-Ibm",
});

const orbitron = Orbitron({
	subsets: ["latin"],
	weight: ["400", "700"],
	variable: "--font-orbitron",
});

const rubik = Rubik({
	subsets: ["latin"],
	weight: ["400", "700"],
	variable: "--font-rubik",
});

const roboto = Roboto({
	subsets: ["latin"],
	weight: ["400", "500", "700"],
	variable: "--font-roboto",
});

export const metadata: Metadata = {
	title: "OSS | PRMS",
	description: "Ease the process of creating sales and inventory reports",
	icons: {
		icon: "printico.ico",
	},
};

export default function RootLayout({
	children,
}: Readonly<{
	children: React.ReactNode;
}>) {
	return (
		<html lang="en">
			<body
				className={cn(
					orbitron.variable,
					ibmPlex.variable,
					rubik.variable,
					roboto.variable
				)}
			>
				{children}
				<Toaster />
			</body>
		</html>
	);
}
