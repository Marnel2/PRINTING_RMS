import React from "react";
import HeaderBox from "../../../../components/HeaderBox";

const SingleSlae = ({ params }: { params: { id: number } }) => {
	const id = params.id;
	return (
		<section>
			<HeaderBox title={`Sales ${id}`} subtext=" "  />
		</section>
	);
};

export default SingleSlae;
