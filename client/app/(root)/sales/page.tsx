import React from "react";
import HeaderBox from "../../../components/HeaderBox";
import Link from "next/link";

const SalesPage = () => {
	return (
		<section>
			<HeaderBox
				title="Sales"
				subtext="Here are the sales that the shop earned"
			/>
			<div className="py-5">
				<Link
					href={`/sales/1`}
					className="font-bold text-primary border border-primary rounded-md p-1"
				>
					click me!
				</Link>
			</div>
		</section>
	);
};

export default SalesPage;
