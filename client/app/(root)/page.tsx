"use client";
import React from "react";
import HeaderBox from "../../components/HeaderBox";
import { getDayToday } from "@/lib";
import InfoBox from "@/components/InfoBox";
import useAuthStore from "@/store/authStore";
import BarChartComponent from "@/components/BarChartComponent";
import PieChartComponent from "@/components/PieChartComponent";

const Dashboard = () => {
	const user = useAuthStore((state) => state.user);
	const dayToday = getDayToday();

	return (
		<section>
			<HeaderBox
				title="Welcome, "
				type="greeting"
				user={user?.name || "Admin"}
				subtext={`It's ${dayToday}, keep pushing forward!`}
			/>
			<div className="w-full mt-10">
				<InfoBox />
				<div className="mt-6 w-full">
					<BarChartComponent />
				</div>
				{/* <div className="mt-6 w-[30%]">
					<PieChartComponent />
				</div> */}
			</div>
		</section>
	);
};

export default Dashboard;
