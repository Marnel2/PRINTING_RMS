import React from "react";
import Navbar from "../../components/Navbar";
import ProtectedRoute from "@/hoc/ProtectedRoute";
import Footer from "@/components/Footer";

interface IMainLayoutProps {
	children: React.ReactNode;
}

const MainLayout = ({ children }: IMainLayoutProps) => {
	return (
		<>
			<ProtectedRoute>
				<main className="flex bg-gray-50 h-full">
					<section className="fixed">
						<Navbar />
					</section>
					<section className="py-10 pl-[350px] pr-[100px] w-full ">
						{children}
					</section>
				</main>
			</ProtectedRoute>
		</>
	);
};

export default MainLayout;
