import React from "react";
import HeaderBox from "../../../components/HeaderBox";
import { Button } from "@/components/ui/button";
import {
	Card,
	CardContent,
	CardDescription,
	CardFooter,
	CardHeader,
	CardTitle,
} from "@/components/ui/card";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import PaperConsumptionForm from "@/components/PaperConsumptionForm";
import ReportTable from "@/components/ReportTable";

const ServicesPage = () => {
	return (
		<section className="h-full font-roboto">
			<HeaderBox
				title="Services"
				subtext="Report every services that you have been done."
			/>
			<Tabs defaultValue="paper" className="w-full mt-8">
				<TabsList className="tab-list bg-white">
					<TabsTrigger value="paper" className="tabs-trigger">
						Paper Printing
					</TabsTrigger>
					<TabsTrigger value="tarpaulin" className="tabs-trigger">
						Tarpaulin
					</TabsTrigger>
					<TabsTrigger value="acrylicPlate" className="tabs-trigger">
						Acrylic Plate
					</TabsTrigger>
					<TabsTrigger value="laminate" className="tabs-trigger">
						Laminate
					</TabsTrigger>
					<TabsTrigger value="mug" className="tabs-trigger">
						Mug
					</TabsTrigger>
					<TabsTrigger value="shirt" className="tabs-trigger">
						Shirt
					</TabsTrigger>
				</TabsList>
				<TabsContent value="paper">
					<Card className="w-full p-5 bg-white">
						<ReportTable title="Paper" />
					</Card>
				</TabsContent>
				<TabsContent value="tarpaulin">
					<Card className="w-full p-5 bg-white">
						<ReportTable title="Tarpaulin" />
					</Card>
				</TabsContent>
			</Tabs>
		</section>
	);
};

export default ServicesPage;
