"use client";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import useAuthStore from "@/store/authStore";
import Loader from "@/components/Loader";
import { useToast } from "@/components/ui/use-toast";

const ProtectedRoute = ({ children }: { children: React.ReactNode }) => {
	const { user, fetchUser, loading, error } = useAuthStore();
	const [initialLoading, setInitialLoading] = useState(true);
	const router = useRouter();

	const { toast } = useToast();

	useEffect(() => {
		const fetchData = async () => {
			if (!user && !loading) {
				await fetchUser();
			}
			setInitialLoading(false);
		};

		if (initialLoading) {
			fetchData();
		}
	}, [user, loading, fetchUser, initialLoading]);

	useEffect(() => {
		if (!initialLoading && !user && !loading) {
			toast({
				title: "Auth Error",
				description: error,
				className: "error-toast",
			});
			router.push("/sign-in");
		}
	}, [user, loading, initialLoading, router]);

	if (initialLoading || (!user && !loading)) {
		return <Loader />;
	}

	return <>{children}</>;
};

export default ProtectedRoute;
