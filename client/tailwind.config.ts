import type { Config } from "tailwindcss";

const config = {
	darkMode: ["class"],
	content: [
		"./pages/**/*.{ts,tsx}",
		"./components/**/*.{ts,tsx}",
		"./app/**/*.{ts,tsx}",
		"./src/**/*.{ts,tsx}",
	],
	prefix: "",
	theme: {
		container: {
			center: true,
			padding: "2rem",
			screens: {
				"2xl": "1400px",
			},
		},
		extend: {
			colors: {
				primary: "#4793AF",
				secondary: "#FFC470",
				tertiary: "#DD5746",
				accent: "#8B322C",
				"black-100": "#707070",
				"black-200": "#444444",
				"black-300": "#000000",
				"white-100": "#F5F5F5",
			},
			fontFamily: {
				ibmPlex: ["var(--font-Ibm)"],
				orbitron: ["var(--font-orbitron)"],
				rubik: ["var(--font-orbitron)"],
				roboto: ["var(--font-roboto)"],
			},
			backgroundImage: {
				"bg-image": "url(/images/bgImage.jpg)",
			},
			keyframes: {
				"accordion-down": {
					from: { height: "0" },
					to: { height: "var(--radix-accordion-content-height)" },
				},
				"accordion-up": {
					from: { height: "var(--radix-accordion-content-height)" },
					to: { height: "0" },
				},
			},
			animation: {
				"accordion-down": "accordion-down 0.2s ease-out",
				"accordion-up": "accordion-up 0.2s ease-out",
			},
		},
	},
	plugins: [require("tailwindcss-animate")],
} satisfies Config;

export default config;
