import { create } from "zustand";
import axios from "../utils/api";

interface User {
	id: string;
	name: string;
	email: string;
}

interface AuthState {
	user: User | null;
	loading: boolean;
	isLoginSucess: boolean;
	error: string | null;
	refreshing: boolean;
	setRefreshing: (value: boolean) => void;
	login: (email: string, password: string) => Promise<void>;
	logout: () => Promise<void>;
	fetchUser: () => Promise<void>;
}

const useAuthStore = create<AuthState>((set) => ({
	user: null,
	loading: false,
	isLoginSucess: false,
	error: null,
	refreshing: false,
	login: async (email, password) => {
		set({ loading: true, error: null });
		try {
			const response = await axios.post("/login", { email, password });
			set({ user: response.data.user, loading: false, isLoginSucess: true });
		} catch (error: any) {
			set({
				error: error.response?.data?.message || "Login failed",
				loading: false,
				isLoginSucess: false,
			});
		}
	},
	logout: async () => {
		set({ loading: true, error: null });
		try {
			await axios.post("/logout");
			set({ user: null, loading: false, isLoginSucess: false, error: null });
		} catch (error: any) {
			set({
				error: error.response?.data?.message || "Logout failed",
				loading: false,
			});
		}
	},
	fetchUser: async () => {
		set({ loading: true, error: null });
		try {
			const response = await axios.get("/me");
			set({ user: response.data.user, loading: false, error: null });
		} catch (error: any) {
			set({
				error: error.response?.data?.message || "Fetch user failed",
				loading: false,
			});
		}
	},
	setRefreshing: (value) => set({ refreshing: value }),
}));

export default useAuthStore;
