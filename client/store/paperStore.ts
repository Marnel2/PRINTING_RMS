import { create } from "zustand";
import axios from "../utils/api";

interface Brand {
	name: string;
}

interface Product {
	brand: Brand;
	name: string;
}

interface IPaperReport {
	id: number;
	createdAt: string;
	updatedAt: string;
	serviceProvided: string;
	quantity: number;
	remarks: string;
	paperId: number;
	product: Product;
}

type AddPaperReportParams = {
	serviceProvided: string;
	quantity: number;
	remarks: string;
	paperId: number;
	createdAt: Date;
};

type PaperBrands = {
	id: number;
	name: string;
	categoryId: number;
};

type PaperCategories = {
	id: number;
	categoryName: string;
};

type Paper = {
	id: number;
	createdAt: Date;
	updatedAt: Date;
	name: string;
	paperCategoryId: number;
	paperBrandsId: number;
	quantityInStocks: number;
	length: number;
	width: number;
	otherSpecs: string | null;
	pricePerSheet: number | null;
};

interface PaperState {
	paperReportList: IPaperReport[] | null;
	paperBrands: PaperBrands[] | null;
	paperCategories: PaperCategories[] | null;
	papers: Paper[] | null;
	getPapers: (paperCategory: number, paperBrand: number) => Promise<void>;
	loading: boolean;
	error: string | null;
	fetchPaperReport: () => Promise<void>;
	addPaperReport: ({
		serviceProvided,
		quantity,
		remarks,
		paperId,
		createdAt,
	}: AddPaperReportParams) => Promise<void>;
	getPaperCategories: () => Promise<void>;
	getPaperBrands: (categoryId: number) => Promise<void>;
}

const usePaperStore = create<PaperState>((set, get) => ({
	paperReportList: null,
	paperBrands: null,
	paperCategories: null,
	papers: null,
	loading: false,
	error: null,
	fetchPaperReport: async () => {
		set({ loading: true, error: null });
		try {
			const response = await axios.get("/paper-report");
			set({
				paperReportList: response?.data?.paperReports,
				loading: false,
				error: null,
			});
		} catch (error: any) {
			set({ error: error.response?.data?.message, loading: false });
		}
	},
	getPaperCategories: async () => {
		set({ loading: true, error: null });
		try {
			const response = await axios.get("/categories");
			set({
				paperCategories: response?.data?.categories,
				loading: false,
				error: null,
			});
		} catch (error: any) {
			set({ error: error.response?.data?.message, loading: false });
		}
	},
	getPaperBrands: async (categoryId: number) => {
		set({ loading: true, error: null });
		try {
			const response = await axios.get(`/get-brand/${categoryId}`);
			set({
				paperBrands: response?.data?.brands,
				loading: false,
				error: null,
			});
		} catch (error: any) {
			set({ error: error.response?.data?.message, loading: false });
		}
	},
	getPapers: async (paperCategory: number, paperBrand: number) => {
		set({ loading: true, error: null });
		try {
			const response = await axios.post("/get-papers", {
				paperCategory,
				paperBrand,
			});
			set({ papers: response.data.papers, error: null, loading: false });
		} catch (error: any) {
			set({ error: error.response?.data?.message, loading: false });
		}
	},
	addPaperReport: async ({
		serviceProvided,
		quantity,
		remarks,
		paperId,
		createdAt,
	}: AddPaperReportParams) => {
		set({ loading: true, error: null });
		try {
			const response = await axios.post("/paper-report", {
				serviceProvided,
				quantity,
				remarks,
				paperId,
				createdAt,
			});

			if (response.data.success) {
				get().fetchPaperReport();
				set({ loading: false, error: null });
			}
		} catch (error: any) {
			set({ error: error.response?.data?.message, loading: false });
		}
	},
}));

export default usePaperStore;
