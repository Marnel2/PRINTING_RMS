"use client";
import { z } from "zod";
import { zodResolver } from "@hookform/resolvers/zod";
import { useForm } from "react-hook-form";
import {
	Form,
	FormMessage,
	FormDescription,
	FormItem,
	FormControl,
} from "./ui/form";
import { CustomField } from "./CustomField";

import { Label } from "@/components/ui/label";
import { RadioGroup, RadioGroupItem } from "@/components/ui/radio-group";
import { Input } from "./ui/input";
import { Textarea } from "./ui/textarea";
import { Button } from "./ui/button";
import { Calendar } from "./ui/calendar";
import { Popover, PopoverContent, PopoverTrigger } from "./ui/popover";
import { CalendarIcon } from "lucide-react";

import { format } from "date-fns";
import { cn } from "@/lib/utils";
import {
	Select,
	SelectContent,
	SelectItem,
	SelectTrigger,
	SelectValue,
} from "./ui/select";
import usePaperStore from "@/store/paperStore";
import { useEffect } from "react";
import { toast } from "./ui/use-toast";

export const formSchema = z.object({
	dateReported: z.date({
		required_error: "Please select a date and time",
		invalid_type_error: "That's not a date!",
	}),
	serviceType: z.enum(["Copy", "Print", "Consumed", "Damaged"]),
	paperType: z.number(),
	paperBrand: z.number(),
	paperCount: z.number(),
	paper: z.number(),
	remarks: z.string(),
});

const PaperConsumptionForm = ({ formTitle }: { formTitle: string }) => {
	const {
		paperBrands,
		paperCategories,
		getPaperBrands,
		getPaperCategories,
		papers,
		getPapers,
		addPaperReport,
		loading,
		error,
	} = usePaperStore();

	const form = useForm<z.infer<typeof formSchema>>({
		resolver: zodResolver(formSchema),
		defaultValues: {
			dateReported: new Date(),
			serviceType: "Copy",
			paperType: 0,
			paperBrand: 0,
			paper: 0,
			paperCount: 0,
			remarks: "",
		},
	});

	const paperType = form.watch("paperType");
	const paperBrand = form.watch("paperBrand");

	useEffect(() => {
		const initializeForm = async () => {
			if (!paperCategories) {
				await getPaperCategories();
			}

			if (paperCategories && paperCategories.length > 0 && !paperType) {
				const initialCategoryId = paperCategories[0].id;
				form.setValue("paperType", initialCategoryId);
				await getPaperBrands(initialCategoryId);
			}
		};

		initializeForm();
	}, [
		paperCategories,
		getPaperCategories,
		form,
		paperType,
		getPaperBrands,
		paperBrands,
	]);

	useEffect(() => {
		const updateBrands = async () => {
			if (paperType) {
				await getPaperBrands(paperType);
				form.setValue("paperBrand", 0);
			}
		};

		updateBrands();
	}, [paperType, getPaperBrands, form]);

	useEffect(() => {
		const updatePapers = async () => {
			if (paperType && paperBrand) {
				await getPapers(paperType, paperBrand);
				form.setValue("paper", 0);
			}
		};

		updatePapers();
	}, [paperType, paperBrand, getPapers, form]);

	const onSubmit = async (values: z.infer<typeof formSchema>) => {
		try {
			const data = {
				serviceProvided: values.serviceType,
				quantity: values.paperCount,
				remarks: values.remarks,
				paperId: values.paper,
				createdAt: values.dateReported,
			};

			await addPaperReport(data);

			form.reset();

			toast({
				title: "Success",
				className: "success-toast",
				description: "Data has been successfuly added",
			});
		} catch (error: any) {
			toast({ title: "Error", description: error, className: "error-toast" });
		}
	};

	return (
		<div>
			<h1 className="title">{formTitle} Consumptions Form</h1>
			<div className="py-5">
				<Form {...form}>
					{" "}
					<form
						onSubmit={form.handleSubmit(onSubmit)}
						className="flex flex-col gap-4 space-y-3"
					>
						<div className="space-y-3">
							<CustomField
								control={form.control}
								name="dateReported"
								formLabel="Date"
								render={({ field }) => (
									<FormItem className="flex flex-col">
										<Popover>
											<PopoverTrigger asChild>
												<FormControl>
													<Button
														variant={"outline"}
														className={cn(
															"w-full pl-3 text-left font-normal hover:bg-white hover:border",
															!field.value && "text-muted-foreground"
														)}
													>
														{field.value ? (
															format(field.value, "PPP")
														) : (
															<span>Pick a date</span>
														)}
														<CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
													</Button>
												</FormControl>
											</PopoverTrigger>
											<PopoverContent
												className="w-auto p-0 bg-primary text-white"
												align="start"
											>
												<Calendar
													mode="single"
													selected={field.value}
													onSelect={field.onChange}
													disabled={(date) =>
														date > new Date() || date < new Date("1900-01-01")
													}
													initialFocus
												/>
											</PopoverContent>
										</Popover>
									</FormItem>
								)}
							/>
						</div>
						<div className="flex gap-2">
							<CustomField
								control={form.control}
								name="paperType"
								formLabel="Paper Type"
								className="w-full"
								render={({ field }) => (
									<Select
										onValueChange={(value) => {
											field.onChange(parseInt(value, 10));
										}}
										value={field.value.toString()}
									>
										<SelectTrigger className="select-field">
											<SelectValue placeholder="Select a type" />
										</SelectTrigger>
										<SelectContent>
											{paperCategories &&
												paperCategories.map((category) => (
													<SelectItem
														key={category.id}
														value={category.id.toString()}
														className="select-item"
													>
														{category.categoryName}
													</SelectItem>
												))}
										</SelectContent>
									</Select>
								)}
							/>
							<CustomField
								control={form.control}
								name="paperBrand"
								formLabel="Brand"
								className="w-full"
								render={({ field }) => (
									<Select
										onValueChange={(value) =>
											field.onChange(parseInt(value, 10))
										}
										value={field.value ? field.value.toString() : ""}
										disabled={!paperType || !paperBrands}
									>
										<SelectTrigger>
											<SelectValue placeholder="Select a brand" />
										</SelectTrigger>
										<SelectContent className="bg-white">
											{paperBrands &&
												paperBrands?.map((brand) => (
													<SelectItem
														key={brand.id}
														value={brand.id.toString()}
														className="select-item"
													>
														{brand.name}
													</SelectItem>
												))}
										</SelectContent>
									</Select>
								)}
							/>
						</div>
						<div>
							<RadioGroup
								defaultValue="copy"
								className="flex justify-start gap-5"
							>
								<div className="radio-item">
									<RadioGroupItem value="Copy" id="copy" />
									<Label htmlFor="copy" className="field-label">
										Photocopy
									</Label>
								</div>
								<div className="radio-item">
									<RadioGroupItem value="Print" id="print" />
									<Label htmlFor="print" className="field-label">
										Print
									</Label>
								</div>
								<div className="radio-item">
									<RadioGroupItem value="Consumed" id="consumed" />
									<Label htmlFor="consumed" className="field-label">
										Consumed
									</Label>
								</div>
								<div className="radio-item">
									<RadioGroupItem value="Damaged" id="damaged" />
									<Label htmlFor="damaged" className="field-label">
										Damaged
									</Label>
								</div>
							</RadioGroup>
						</div>
						<div className="space-y-3">
							<div className="flex flex-row-reverse gap-2">
								<CustomField
									control={form.control}
									name="paperCount"
									formLabel="Paper"
									render={({ field }) => (
										<Input
											{...field}
											className="input-field"
											type="number"
											onChange={(e) => {
												field.onChange(Number(e.target.value));
											}}
											value={field.value}
										/>
									)}
								/>
								<CustomField
									control={form.control}
									name="paper"
									formLabel="Paper"
									className="w-full"
									render={({ field }) => (
										<Select
											onValueChange={(value) =>
												field.onChange(parseInt(value, 10))
											}
											value={field.value ? field.value.toString() : ""}
											disabled={(!paperType && !paperBrand) || !papers}
										>
											<SelectTrigger>
												<SelectValue placeholder="Select a paper" />
											</SelectTrigger>
											<SelectContent className="bg-white">
												{papers?.map((paper) => (
													<SelectItem
														key={paper.id}
														value={paper.id.toString()}
														className="select-item"
													>
														{paper.name}{" "}
														<span className="text-gray-500">
															({paper.quantityInStocks} left)
														</span>
													</SelectItem>
												))}
											</SelectContent>
										</Select>
									)}
								/>
							</div>
							<CustomField
								control={form.control}
								name="remarks"
								formLabel="Remarks"
								render={({ field }: any) => (
									<Textarea {...field} className="input-field" />
								)}
							/>
						</div>
						<div>
							<Button type="submit" className="submit-button capitalize">
								Save
							</Button>
						</div>
					</form>
				</Form>
			</div>
		</div>
	);
};

export default PaperConsumptionForm;
