"use client";
"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.formSchema = void 0;
var zod_1 = require("zod");
var zod_2 = require("@hookform/resolvers/zod");
var react_hook_form_1 = require("react-hook-form");
var form_1 = require("./ui/form");
var CustomField_1 = require("./CustomField");
var label_1 = require("@/components/ui/label");
var radio_group_1 = require("@/components/ui/radio-group");
var input_1 = require("./ui/input");
var textarea_1 = require("./ui/textarea");
var button_1 = require("./ui/button");
var calendar_1 = require("./ui/calendar");
var popover_1 = require("./ui/popover");
var lucide_react_1 = require("lucide-react");
var date_fns_1 = require("date-fns");
var utils_1 = require("@/lib/utils");
var select_1 = require("./ui/select");
var paperStore_1 = require("@/store/paperStore");
var react_1 = require("react");
var use_toast_1 = require("./ui/use-toast");
exports.formSchema = zod_1.z.object({
    dateReported: zod_1.z.date({
        required_error: "Please select a date and time",
        invalid_type_error: "That's not a date!"
    }),
    serviceType: zod_1.z["enum"](["Copy", "Print", "Consumed", "Damaged"]),
    paperType: zod_1.z.number(),
    paperBrand: zod_1.z.number(),
    paperCount: zod_1.z.number(),
    paper: zod_1.z.number(),
    remarks: zod_1.z.string()
});
var PaperConsumptionForm = function (_a) {
    var formTitle = _a.formTitle;
    var _b = paperStore_1["default"](), paperBrands = _b.paperBrands, paperCategories = _b.paperCategories, getPaperBrands = _b.getPaperBrands, getPaperCategories = _b.getPaperCategories, papers = _b.papers, getPapers = _b.getPapers, addPaperReport = _b.addPaperReport, loading = _b.loading, error = _b.error;
    var form = react_hook_form_1.useForm({
        resolver: zod_2.zodResolver(exports.formSchema),
        defaultValues: {
            dateReported: new Date(),
            serviceType: "Copy",
            paperType: 0,
            paperBrand: 0,
            paper: 0,
            paperCount: 0,
            remarks: ""
        }
    });
    var paperType = form.watch("paperType");
    var paperBrand = form.watch("paperBrand");
    react_1.useEffect(function () {
        var initializeForm = function () { return __awaiter(void 0, void 0, void 0, function () {
            var initialCategoryId;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!!paperCategories) return [3 /*break*/, 2];
                        return [4 /*yield*/, getPaperCategories()];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2:
                        if (!(paperCategories && paperCategories.length > 0 && !paperType)) return [3 /*break*/, 4];
                        initialCategoryId = paperCategories[0].id;
                        form.setValue("paperType", initialCategoryId);
                        return [4 /*yield*/, getPaperBrands(initialCategoryId)];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        }); };
        initializeForm();
    }, [
        paperCategories,
        getPaperCategories,
        form,
        paperType,
        getPaperBrands,
        paperBrands,
    ]);
    react_1.useEffect(function () {
        var updateBrands = function () { return __awaiter(void 0, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!paperType) return [3 /*break*/, 2];
                        return [4 /*yield*/, getPaperBrands(paperType)];
                    case 1:
                        _a.sent();
                        form.setValue("paperBrand", 0);
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        }); };
        updateBrands();
    }, [paperType, getPaperBrands, form]);
    react_1.useEffect(function () {
        var updatePapers = function () { return __awaiter(void 0, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(paperType && paperBrand)) return [3 /*break*/, 2];
                        return [4 /*yield*/, getPapers(paperType, paperBrand)];
                    case 1:
                        _a.sent();
                        form.setValue("paper", 0);
                        _a.label = 2;
                    case 2: return [2 /*return*/];
                }
            });
        }); };
        updatePapers();
    }, [paperType, paperBrand, getPapers, form]);
    var onSubmit = function (values) { return __awaiter(void 0, void 0, void 0, function () {
        var data, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    _a.trys.push([0, 2, , 3]);
                    data = {
                        serviceProvided: values.serviceType,
                        quantity: values.paperCount,
                        remarks: values.remarks,
                        paperId: values.paper,
                        createdAt: values.dateReported
                    };
                    return [4 /*yield*/, addPaperReport(data)];
                case 1:
                    _a.sent();
                    form.reset();
                    use_toast_1.toast({
                        title: "Success",
                        className: "success-toast",
                        description: "Data has been successfuly added"
                    });
                    return [3 /*break*/, 3];
                case 2:
                    error_1 = _a.sent();
                    use_toast_1.toast({ title: "Error", description: error_1, className: "error-toast" });
                    return [3 /*break*/, 3];
                case 3: return [2 /*return*/];
            }
        });
    }); };
    return (React.createElement("div", null,
        React.createElement("h1", { className: "title" },
            formTitle,
            " Consumptions Form"),
        React.createElement("div", { className: "py-5" },
            React.createElement(form_1.Form, __assign({}, form),
                " ",
                React.createElement("form", { onSubmit: form.handleSubmit(onSubmit), className: "flex flex-col gap-4 space-y-3" },
                    React.createElement("div", { className: "space-y-3" },
                        React.createElement(CustomField_1.CustomField, { control: form.control, name: "dateReported", formLabel: "Date", render: function (_a) {
                                var field = _a.field;
                                return (React.createElement(form_1.FormItem, { className: "flex flex-col" },
                                    React.createElement(popover_1.Popover, null,
                                        React.createElement(popover_1.PopoverTrigger, { asChild: true },
                                            React.createElement(form_1.FormControl, null,
                                                React.createElement(button_1.Button, { variant: "outline", className: utils_1.cn("w-full pl-3 text-left font-normal hover:bg-white hover:border", !field.value && "text-muted-foreground") },
                                                    field.value ? (date_fns_1.format(field.value, "PPP")) : (React.createElement("span", null, "Pick a date")),
                                                    React.createElement(lucide_react_1.CalendarIcon, { className: "ml-auto h-4 w-4 opacity-50" })))),
                                        React.createElement(popover_1.PopoverContent, { className: "w-auto p-0 bg-primary text-white", align: "start" },
                                            React.createElement(calendar_1.Calendar, { mode: "single", selected: field.value, onSelect: field.onChange, disabled: function (date) {
                                                    return date > new Date() || date < new Date("1900-01-01");
                                                }, initialFocus: true })))));
                            } })),
                    React.createElement("div", { className: "flex gap-2" },
                        React.createElement(CustomField_1.CustomField, { control: form.control, name: "paperType", formLabel: "Paper Type", className: "w-full", render: function (_a) {
                                var field = _a.field;
                                return (React.createElement(select_1.Select, { onValueChange: function (value) {
                                        field.onChange(parseInt(value, 10));
                                    }, value: field.value.toString() },
                                    React.createElement(select_1.SelectTrigger, { className: "select-field" },
                                        React.createElement(select_1.SelectValue, { placeholder: "Select a type" })),
                                    React.createElement(select_1.SelectContent, null, paperCategories &&
                                        paperCategories.map(function (category) { return (React.createElement(select_1.SelectItem, { key: category.id, value: category.id.toString(), className: "select-item" }, category.categoryName)); }))));
                            } }),
                        React.createElement(CustomField_1.CustomField, { control: form.control, name: "paperBrand", formLabel: "Brand", className: "w-full", render: function (_a) {
                                var field = _a.field;
                                return (React.createElement(select_1.Select, { onValueChange: function (value) {
                                        return field.onChange(parseInt(value, 10));
                                    }, value: field.value ? field.value.toString() : "", disabled: !paperType || !paperBrands },
                                    React.createElement(select_1.SelectTrigger, null,
                                        React.createElement(select_1.SelectValue, { placeholder: "Select a brand" })),
                                    React.createElement(select_1.SelectContent, { className: "bg-white" }, paperBrands && (paperBrands === null || paperBrands === void 0 ? void 0 : paperBrands.map(function (brand) { return (React.createElement(select_1.SelectItem, { key: brand.id, value: brand.id.toString(), className: "select-item" }, brand.name)); })))));
                            } })),
                    React.createElement("div", null,
                        React.createElement(radio_group_1.RadioGroup, { defaultValue: "copy", className: "flex justify-start gap-5" },
                            React.createElement("div", { className: "radio-item" },
                                React.createElement(radio_group_1.RadioGroupItem, { value: "Copy", id: "copy" }),
                                React.createElement(label_1.Label, { htmlFor: "copy", className: "field-label" }, "Photocopy")),
                            React.createElement("div", { className: "radio-item" },
                                React.createElement(radio_group_1.RadioGroupItem, { value: "Print", id: "print" }),
                                React.createElement(label_1.Label, { htmlFor: "print", className: "field-label" }, "Print")),
                            React.createElement("div", { className: "radio-item" },
                                React.createElement(radio_group_1.RadioGroupItem, { value: "Consumed", id: "consumed" }),
                                React.createElement(label_1.Label, { htmlFor: "consumed", className: "field-label" }, "Consumed")),
                            React.createElement("div", { className: "radio-item" },
                                React.createElement(radio_group_1.RadioGroupItem, { value: "Damaged", id: "damaged" }),
                                React.createElement(label_1.Label, { htmlFor: "damaged", className: "field-label" }, "Damaged")))),
                    React.createElement("div", { className: "space-y-3" },
                        React.createElement("div", { className: "flex flex-row-reverse gap-2" },
                            React.createElement(CustomField_1.CustomField, { control: form.control, name: "paperCount", formLabel: "Paper", render: function (_a) {
                                    var field = _a.field;
                                    return (React.createElement(input_1.Input, __assign({}, field, { className: "input-field", type: "number", onChange: function (e) {
                                            field.onChange(Number(e.target.value));
                                        }, value: field.value })));
                                } }),
                            React.createElement(CustomField_1.CustomField, { control: form.control, name: "paper", formLabel: "Paper", className: "w-full", render: function (_a) {
                                    var field = _a.field;
                                    return (React.createElement(select_1.Select, { onValueChange: function (value) {
                                            return field.onChange(parseInt(value, 10));
                                        }, value: field.value ? field.value.toString() : "", disabled: (!paperType && !paperBrand) || !papers },
                                        React.createElement(select_1.SelectTrigger, null,
                                            React.createElement(select_1.SelectValue, { placeholder: "Select a paper" })),
                                        React.createElement(select_1.SelectContent, { className: "bg-white" }, papers === null || papers === void 0 ? void 0 : papers.map(function (paper) { return (React.createElement(select_1.SelectItem, { key: paper.id, value: paper.id.toString(), className: "select-item" },
                                            paper.name,
                                            " ",
                                            React.createElement("span", { className: "text-gray-500" },
                                                "(",
                                                paper.quantityInStocks,
                                                " left)"))); }))));
                                } })),
                        React.createElement(CustomField_1.CustomField, { control: form.control, name: "remarks", formLabel: "Remarks", render: function (_a) {
                                var field = _a.field;
                                return (React.createElement(textarea_1.Textarea, __assign({}, field, { className: "input-field" })));
                            } })),
                    React.createElement("div", null,
                        React.createElement(button_1.Button, { type: "submit", className: "submit-button capitalize" }, "Save")))))));
};
exports["default"] = PaperConsumptionForm;
