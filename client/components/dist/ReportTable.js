"use client";
"use strict";
exports.__esModule = true;
var table_1 = require("@/components/ui/table");
var link_1 = require("next/link");
var CustomDialog_1 = require("./CustomDialog");
var PaperConsumptionForm_1 = require("./PaperConsumptionForm");
var paperStore_1 = require("@/store/paperStore");
var react_1 = require("react");
var date_fns_1 = require("date-fns");
var ReportTable = function (_a) {
    var title = _a.title;
    var paperReport = paperStore_1["default"](function (state) { return state.paperReportList; });
    var fetchPaperReport = paperStore_1["default"](function (state) { return state.fetchPaperReport; });
    react_1.useEffect(function () {
        fetchPaperReport();
    }, [fetchPaperReport]);
    return (React.createElement(React.Fragment, null,
        React.createElement("div", { className: "flex justify-between border-b-2 w-full pb-4" },
            React.createElement("h1", { className: "title" },
                title,
                " Consumptions Report"),
            React.createElement(CustomDialog_1["default"], null,
                React.createElement(PaperConsumptionForm_1["default"], { formTitle: title }))),
        React.createElement(table_1.Table, null,
            React.createElement(table_1.TableCaption, null, "A list of your recent services"),
            React.createElement(table_1.TableHeader, null,
                React.createElement(table_1.TableRow, null,
                    React.createElement(table_1.TableHead, null, "ID"),
                    React.createElement(table_1.TableHead, null, "Service"),
                    React.createElement(table_1.TableHead, null, "Paper"),
                    React.createElement(table_1.TableHead, null, "Brand"),
                    React.createElement(table_1.TableHead, null, "Quantity"),
                    React.createElement(table_1.TableHead, null, "Remarks"),
                    React.createElement(table_1.TableHead, null, "Recorded At"),
                    React.createElement(table_1.TableHead, null, "Actions"))),
            React.createElement(table_1.TableBody, null, paperReport &&
                paperReport.map(function (report) { return (React.createElement(table_1.TableRow, { key: report.id },
                    React.createElement(table_1.TableCell, { className: "font-medium" },
                        "INV",
                        report.id),
                    React.createElement(table_1.TableCell, null, report.serviceProvided),
                    React.createElement(table_1.TableCell, null, report.product.name),
                    React.createElement(table_1.TableCell, null, report.product.brand.name),
                    React.createElement(table_1.TableCell, null, report.quantity),
                    React.createElement(table_1.TableCell, null, report.remarks),
                    React.createElement(table_1.TableCell, null, date_fns_1.format(report.createdAt, "MMM dd, yyyy, h:mm a")),
                    React.createElement(table_1.TableCell, { className: "flex gap-1" },
                        React.createElement(link_1["default"], { href: "/" }, "View"),
                        React.createElement(link_1["default"], { href: "/" }, "Edit"),
                        React.createElement(link_1["default"], { href: "/" }, "Delete")))); })))));
};
exports["default"] = ReportTable;
