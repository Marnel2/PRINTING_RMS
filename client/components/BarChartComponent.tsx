"use client";

import * as React from "react";
import { Bar, BarChart, CartesianGrid, XAxis } from "recharts";

import {
	Card,
	CardContent,
	CardDescription,
	CardHeader,
	CardTitle,
} from "@/components/ui/card";
import {
	ChartConfig,
	ChartContainer,
	ChartTooltip,
	ChartTooltipContent,
} from "@/components/ui/chart";
const chartData = [
	{ date: "2024-04-01", print: 222, copy: 150 },
	{ date: "2024-04-02", print: 97, copy: 180 },
	{ date: "2024-04-03", print: 167, copy: 120 },
	{ date: "2024-04-04", print: 242, copy: 260 },
	{ date: "2024-04-05", print: 373, copy: 290 },
	{ date: "2024-04-06", print: 301, copy: 340 },
	{ date: "2024-04-07", print: 245, copy: 180 },
	{ date: "2024-04-08", print: 409, copy: 320 },
	{ date: "2024-04-09", print: 59, copy: 110 },
	{ date: "2024-04-10", print: 261, copy: 190 },
	{ date: "2024-04-11", print: 327, copy: 350 },
	{ date: "2024-04-12", print: 292, copy: 210 },
	{ date: "2024-04-13", print: 342, copy: 380 },
	{ date: "2024-04-14", print: 137, copy: 220 },
	{ date: "2024-04-15", print: 120, copy: 170 },
	{ date: "2024-04-16", print: 138, copy: 190 },
	{ date: "2024-04-17", print: 446, copy: 360 },
	{ date: "2024-04-18", print: 364, copy: 410 },
	{ date: "2024-04-19", print: 243, copy: 180 },
	{ date: "2024-04-20", print: 89, copy: 150 },
	{ date: "2024-04-21", print: 137, copy: 200 },
	{ date: "2024-04-22", print: 224, copy: 170 },
	{ date: "2024-04-23", print: 138, copy: 230 },
	{ date: "2024-04-24", print: 387, copy: 290 },
	{ date: "2024-04-25", print: 215, copy: 250 },
	{ date: "2024-04-26", print: 75, copy: 130 },
	{ date: "2024-04-27", print: 383, copy: 420 },
	{ date: "2024-04-28", print: 122, copy: 180 },
	{ date: "2024-04-29", print: 315, copy: 240 },
	{ date: "2024-04-30", print: 454, copy: 380 },
	{ date: "2024-05-01", print: 165, copy: 220 },
	{ date: "2024-05-02", print: 293, copy: 310 },
	{ date: "2024-05-03", print: 247, copy: 190 },
	{ date: "2024-05-04", print: 385, copy: 420 },
	{ date: "2024-05-05", print: 481, copy: 390 },
	{ date: "2024-05-06", print: 498, copy: 520 },
	{ date: "2024-05-07", print: 388, copy: 300 },
	{ date: "2024-05-08", print: 149, copy: 210 },
	{ date: "2024-05-09", print: 227, copy: 180 },
	{ date: "2024-05-10", print: 293, copy: 330 },
	{ date: "2024-05-11", print: 335, copy: 270 },
	{ date: "2024-05-12", print: 197, copy: 240 },
	{ date: "2024-05-13", print: 197, copy: 160 },
	{ date: "2024-05-14", print: 448, copy: 490 },
	{ date: "2024-05-15", print: 473, copy: 380 },
	{ date: "2024-05-16", print: 338, copy: 400 },
	{ date: "2024-05-17", print: 499, copy: 420 },
	{ date: "2024-05-18", print: 315, copy: 350 },
	{ date: "2024-05-19", print: 235, copy: 180 },
	{ date: "2024-05-20", print: 177, copy: 230 },
	{ date: "2024-05-21", print: 82, copy: 140 },
	{ date: "2024-05-22", print: 81, copy: 120 },
	{ date: "2024-05-23", print: 252, copy: 290 },
	{ date: "2024-05-24", print: 294, copy: 220 },
	{ date: "2024-05-25", print: 201, copy: 250 },
	{ date: "2024-05-26", print: 213, copy: 170 },
	{ date: "2024-05-27", print: 420, copy: 460 },
	{ date: "2024-05-28", print: 233, copy: 190 },
	{ date: "2024-05-29", print: 78, copy: 130 },
	{ date: "2024-05-30", print: 340, copy: 280 },
	{ date: "2024-05-31", print: 178, copy: 230 },
	{ date: "2024-06-01", print: 178, copy: 200 },
	{ date: "2024-06-02", print: 470, copy: 410 },
	{ date: "2024-06-03", print: 103, copy: 160 },
	{ date: "2024-06-04", print: 439, copy: 380 },
	{ date: "2024-06-05", print: 88, copy: 140 },
	{ date: "2024-06-06", print: 294, copy: 250 },
	{ date: "2024-06-07", print: 323, copy: 370 },
	{ date: "2024-06-08", print: 385, copy: 320 },
	{ date: "2024-06-09", print: 438, copy: 480 },
	{ date: "2024-06-10", print: 155, copy: 200 },
	{ date: "2024-06-11", print: 92, copy: 150 },
	{ date: "2024-06-12", print: 492, copy: 420 },
	{ date: "2024-06-13", print: 81, copy: 130 },
	{ date: "2024-06-14", print: 426, copy: 380 },
	{ date: "2024-06-15", print: 307, copy: 350 },
	{ date: "2024-06-16", print: 371, copy: 310 },
	{ date: "2024-06-17", print: 475, copy: 520 },
	{ date: "2024-06-18", print: 107, copy: 170 },
	{ date: "2024-06-19", print: 341, copy: 290 },
	{ date: "2024-06-20", print: 408, copy: 450 },
	{ date: "2024-06-21", print: 169, copy: 210 },
	{ date: "2024-06-22", print: 317, copy: 270 },
	{ date: "2024-06-23", print: 480, copy: 530 },
	{ date: "2024-06-24", print: 132, copy: 180 },
	{ date: "2024-06-25", print: 141, copy: 190 },
	{ date: "2024-06-26", print: 434, copy: 380 },
	{ date: "2024-06-27", print: 448, copy: 490 },
	{ date: "2024-06-28", print: 149, copy: 200 },
	{ date: "2024-06-29", print: 103, copy: 160 },
	{ date: "2024-06-30", print: 446, copy: 400 },
];

const chartConfig = {
	views: {
		label: "Page Views",
	},
	print: {
		label: "Print",
		color: "hsl(var(--chart-1))",
	},
	copy: {
		label: "Copy",
		color: "hsl(var(--chart-2))",
	},
} satisfies ChartConfig;

function BarChartComponent() {
	const [activeChart, setActiveChart] =
		React.useState<keyof typeof chartConfig>("print");

	const total = React.useMemo(
		() => ({
			print: chartData.reduce((acc, curr) => acc + curr.print, 0),
			copy: chartData.reduce((acc, curr) => acc + curr.copy, 0),
		}),
		[]
	);

	return (
		<Card className="bg-white">
			<CardHeader className="flex flex-col items-stretch space-y-0 border-b p-0 sm:flex-row">
				<div className="flex flex-1 flex-col justify-center gap-1 px-6 py-5 sm:py-6">
					<CardTitle className="title">Paper - Consumptions Report</CardTitle>
					<CardDescription>
						Showing total visitors for the last 3 months
					</CardDescription>
				</div>
				<div className="flex">
					{["print", "copy"].map((key) => {
						const chart = key as keyof typeof chartConfig;
						return (
							<button
								key={chart}
								data-active={activeChart === chart}
								className="relative z-30 flex flex-1 flex-col justify-center gap-1 border-t px-6 py-4 text-left even:border-l data-[active=true]:bg-muted/50 sm:border-l sm:border-t-0 sm:px-8 sm:py-6"
								onClick={() => setActiveChart(chart)}
							>
								<span className="text-xs text-muted-foreground">
									{chartConfig[chart].label}
								</span>
								<span className="text-lg font-bold leading-none sm:text-3xl">
									{total[key as keyof typeof total].toLocaleString()}
								</span>
							</button>
						);
					})}
				</div>
			</CardHeader>
			<CardContent className="px-2 sm:p-6">
				<ChartContainer
					config={chartConfig}
					className="aspect-auto h-[250px] w-full"
				>
					<BarChart
						accessibilityLayer
						data={chartData}
						margin={{
							left: 12,
							right: 12,
						}}
					>
						<CartesianGrid vertical={false} />
						<XAxis
							dataKey="date"
							tickLine={false}
							axisLine={false}
							tickMargin={8}
							minTickGap={32}
							tickFormatter={(value) => {
								const date = new Date(value);
								return date.toLocaleDateString("en-US", {
									month: "short",
									day: "numeric",
								});
							}}
						/>
						<ChartTooltip
							content={
								<ChartTooltipContent
									className="w-[150px] bg-white"
									nameKey="views"
									labelFormatter={(value) => {
										return new Date(value).toLocaleDateString("en-US", {
											month: "short",
											day: "numeric",
											year: "numeric",
										});
									}}
								/>
							}
						/>
						<Bar dataKey={activeChart} fill={`var(--color-${activeChart})`} />
					</BarChart>
				</ChartContainer>
			</CardContent>
		</Card>
	);
}

export default BarChartComponent;
