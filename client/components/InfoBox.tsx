import React from "react";
import { Card } from "./ui/card";

const InfoBox = () => {
	return (
		<div className="flex gap-4">
			<Card className="info-box">
				<h2>Sales</h2>
			</Card>
			<Card className="info-box ">
				<h2>Loss</h2>
			</Card>
			<Card className="info-box ">
				<h2>Consumptions</h2>
			</Card>
			<Card className="info-box ">
				<h2>Customers</h2>
			</Card>
		</div>
	);
};

export default InfoBox;
