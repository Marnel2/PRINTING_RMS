"use client";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import { usePathname, useRouter } from "next/navigation";
import { navLinks } from "../lib";
import useAuthStore from "../store/authStore";
import Loader from "./Loader";
import { LiaSignOutAltSolid } from "react-icons/lia";

const Navbar = () => {
	const pathName = usePathname();
	const logout = useAuthStore((state) => state.logout);
	const loading = useAuthStore((state) => state.loading);
	const router = useRouter();

	const handleLogout = async () => {
		await logout();
		router.push("/sign-in");
	};

	return (
		<aside className="sidebar">
			<div className="flex size-full flex-col gap-4">
				<Link href="/" className="sidebar-logo">
					<div>
						<Image
							src="/images/temp-logo.png"
							width={38}
							height={38}
							alt="logo"
						/>
					</div>
					<h2 className="font-semibold text-[24px] font-Ibm">PRMS</h2>
				</Link>
				<nav className="sidebar-nav">
					<ul className="sidebar-nav_elements">
						{navLinks.map((item) => {
							const isActive =
								pathName === item.url || pathName?.startsWith(`${item.url}/`);

							return (
								<Link
									href={item.url}
									key={item.label}
									className={`sidebar-nav_element group ${
										isActive && "activeLinks"
									} navbarLinks`}
								>
									{item.label}
								</Link>
							);
						})}
					</ul>
					<div className="w-full">
						<button
							className="signout flex items-center justify-center gap-2"
							onClick={handleLogout}
						>
							<span>Sign out</span>
							<span>
								<LiaSignOutAltSolid className="text-[18px]" />
							</span>
						</button>
					</div>
				</nav>
			</div>
		</aside>
	);
};

export default Navbar;
