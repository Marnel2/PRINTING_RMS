import { Button } from "@/components/ui/button";
import {
	Dialog,
	DialogContent,
	DialogTitle,
	DialogTrigger,
} from "@/components/ui/dialog";

const CustomDialog = ({ children }: { children: React.ReactNode }) => {
	return (
		<Dialog>
			<DialogTitle className="hidden"></DialogTitle>
			<DialogTrigger asChild>
				<Button variant="outline" className="dialog-trigger hover:bg-primary">
					ADD NEW
				</Button>
			</DialogTrigger>
			<DialogContent className=" bg-white">{children}</DialogContent>
		</Dialog>
	);
};

export default CustomDialog;
