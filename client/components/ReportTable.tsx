"use client";
import {
	Table,
	TableBody,
	TableCaption,
	TableCell,
	TableHead,
	TableHeader,
	TableRow,
} from "@/components/ui/table";
import Link from "next/link";
import CustomDialog from "./CustomDialog";
import PaperConsumptionForm from "./PaperConsumptionForm";
import usePaperStore from "@/store/paperStore";
import { useEffect } from "react";
import { format } from "date-fns";

const ReportTable = ({ title }: { title: string }) => {
	const paperReport = usePaperStore((state) => state.paperReportList);
	const fetchPaperReport = usePaperStore((state) => state.fetchPaperReport);

	useEffect(() => {
		fetchPaperReport();
	}, [fetchPaperReport]);

	return (
		<>
			<div className="flex justify-between border-b-2 w-full pb-4">
				<h1 className="title">{title} Consumptions Report</h1>
				<CustomDialog>
					<PaperConsumptionForm formTitle={title} />
				</CustomDialog>
			</div>
			<Table>
				<TableCaption>A list of your recent services</TableCaption>
				<TableHeader>
					<TableRow>
						<TableHead>ID</TableHead>
						<TableHead>Service</TableHead>
						<TableHead>Paper</TableHead>
						<TableHead>Brand</TableHead>
						<TableHead>Quantity</TableHead>
						<TableHead>Remarks</TableHead>
						<TableHead>Recorded At</TableHead>
						<TableHead>Actions</TableHead>
					</TableRow>
				</TableHeader>
				<TableBody>
					{paperReport &&
						paperReport.map((report) => (
							<TableRow key={report.id}>
								<TableCell className="font-medium">INV{report.id}</TableCell>
								<TableCell>{report.serviceProvided}</TableCell>
								<TableCell>{report.product.name}</TableCell>
								<TableCell>{report.product.brand.name}</TableCell>
								<TableCell>{report.quantity}</TableCell>
								<TableCell>{report.remarks}</TableCell>
								<TableCell>
									{format(report.createdAt, "MMM dd, yyyy, h:mm a")}
								</TableCell>
								<TableCell className="flex gap-1">
									<Link href="/">View</Link>
									<Link href="/">Edit</Link>
									<Link href="/">Delete</Link>
								</TableCell>
							</TableRow>
						))}
				</TableBody>
			</Table>
		</>
	);
};

export default ReportTable;
