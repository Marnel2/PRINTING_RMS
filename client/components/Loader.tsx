import React from "react";
import Image from "next/image";

const Loader = () => {
	return (
		<div
			className="w-full h-screen flex items-center justify-center bg-[#DCE7E9] transition-all"
			aria-disabled
		>
			<Image
				src="/images/loader.gif"
				width={300}
				height={300}
				alt="loader"
				unoptimized
				priority
			/>
		</div>
	);
};

export default Loader;
