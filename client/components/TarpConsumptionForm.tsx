import React from "react";

const TarpConsumptionForm = () => {
	return (
		<div className="min-w-[500px] border border-blue rounded-md p-3 shadow-sm">
			<h1 className="font-orbitron font-semibold text-[25px]">
				Tarpaulin Media Consumptions
			</h1>
		</div>
	);
};

export default TarpConsumptionForm;
