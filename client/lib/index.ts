export const navLinks = [
	{
		label: "Dashboard",
		url: "/",
	},
	// {
	// 	label: "Tasks",
	// 	url: "/",
	// },
	// {
	// 	label: "Sales",
	// 	url: "/sales",
	// },
	{
		label: "Services",
		url: "/services",
	},
];

export const getDayToday = () => {
	const dateToday = new Date().getDay();

	const daysOfWeek = [
		"Sunday",
		"Monday",
		"Tuesday",
		"Wednesday",
		"Thursday",
		"Friday",
		"Saturday",
	];

	return daysOfWeek[dateToday];
};
