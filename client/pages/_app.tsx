"use client";
import useAuthStore from "@/store/authStore";
import { AppProps } from "next/app";
import React, { useEffect } from "react";

const MyApp = ({ Component, pageProps }: AppProps) => {
	const { user, fetchUser } = useAuthStore();

	useEffect(() => {
		if (!user) {
			fetchUser();
		}
	}, [user, fetchUser]);

	return <Component {...pageProps} />;
};

export default MyApp;
