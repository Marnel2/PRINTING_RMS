import { NextApiRequest, NextApiResponse } from "next";
import axios from "../../utils/api";

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
	const { method, url } = req;

	switch (method) {
		case "POST":
			if (url?.includes("login")) {
				await axios
					.post("/login", req.body, { headers: req.headers })
					.then((response) => {
						const setCookieHeader = response.headers["set-cookie"];
						if (setCookieHeader) {
							res.setHeader("set-cookie", setCookieHeader);
						}
						res.status(200).json(response.data);
					})
					.catch((error) =>
						res.status(error.response.status).json(error.response.data)
					);
			} else if (url?.includes("logout")) {
				await axios
					.post("/logout", {}, { headers: req.headers })
					.then((response) => {
						const setCookieHeader = response.headers["set-cookie"];
						if (setCookieHeader) {
							res.setHeader("set-cookie", setCookieHeader);
						}
						res.status(200).json(response.data);
					})
					.catch((error) =>
						res.status(error.response.status).json(error.response.data)
					);
			} else if (url?.includes("refresh")) {
				await axios
					.post("/refresh", {}, { headers: req.headers })
					.then((response) => {
						const setCookieHeader = response.headers["set-cookie"];
						if (setCookieHeader) {
							res.setHeader("set-cookie", setCookieHeader);
						}
						res.status(200).json(response.data);
					})
					.catch((error) =>
						res.status(error.response.status).json(error.response.data)
					);
			}
			break;
		case "GET":
			if (url?.includes("me")) {
				await axios
					.get("/auth/me", { headers: req.headers })
					.then((response) => res.status(200).json(response.data))
					.catch((error) =>
						res.status(error.response.status).json(error.response.data)
					);
			}
			break;
		default:
			res.setHeader("Allow", ["POST", "GET"]);
			res.status(405).end(`Method ${method} Not Allowed`);
	}
};

export default handler;
