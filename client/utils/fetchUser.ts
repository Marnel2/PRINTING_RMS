import axios from "axios";

export const fetchUser = async () => {
	const { data } = await axios.get("/me");

	return data.user;
};
